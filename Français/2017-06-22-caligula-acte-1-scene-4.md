---
layout: post
title: "Caligula, Acte 1 Scène 4, Camus"
excerpt: "Hélicon et Caligula, une relation privilégiée"
categories: francais
author: fliife
tags: fr5
---

## Questions possibles:

> Quel est l’intérêt dramatique de la scène ?

> Quelles sont les relations qui existent entre Caligula et Hélicon ? Comment se manifestent-elles ?

> Hélicon est-il un hypocrite ?

> Caligula est-il fou ?

> Comment Camus met-il en scène la métamorphose de Caligula ?

> Comment s’exprime, ici, le mal de vivre de Caligula ?

> Comment Caligula exprime-t- il sa mélancolie ?

> Quelle image de Caligula cette scène propose-t- elle ?

> Caligula ressemble-t- il au portrait qu’en font les patriciens dans la première scène ?

> Dans cette scène, Caligula apparaît-il comme un personnage antipathique ?

## Plan proposé

- **I - La découverte du héros éponyme: le ressenti de Caligula**
    + a) comportement instable
        - Rythme saccadé
        - Didascalies contradictoires: «ton neutre» → «éclat soudain» → «douceur»
        - Gestuelle qui traduit l'agitation
    + b) Expression d'un mal de vivre qui frôle la folie
        - Discours obsessionnel: répétitions («lune», «rien») + extrémisme : «jusqu'au bout»
        - Ressenti négatif: expression de la mélancolie et quête de l'idéal.
            + Caligula exprime sa révolte
            + Passage de l'affirmation subjective à péremptoire indique que caligula prétendra à aider l'humanité à prendre conscience de la vérité
        - Quête poétique surprenante et vouée à l'échec: la lune
    + Bilan: Caligula proche des romantiques: paysage lunaire, expression du rêve et de la mélancolie. Le spectateur peut éprouver de la sympathie envers Caligula
- **II - Un personnage en pleine métamorphose**
    + a) L'éveil d'une conscience
        1. Motif de la fugue
            - Discours qui reflète la rupture caligula manipulable/caligula révolté
            - Mort de Drusilla n'est pas la cause, mais simplement le déclencheur
            - «**_Les hommes meurent et ils ne sont pas heureux_**»
        2. Vérité de Caligula
            - Conjonction «et» sert de connecteur logique
            - Découverte de Caligula: la vie, puisqu'indissociable de la mort, est telle que tout bonheur est impossible.
    + b) Les prémices de la tyrannie
        1. Langage de la certitude
            - Language qui exprime la certitude: «maintenant je sais», ...
            - Caligula se juge au dessus de l'humanité ordinaire: démesure, hubris.
        2. Programme pédagogique: naissance d'un tyran
            - Veut jouer le rôle d'un éveilleur de conscience
            - «tout autour de moi, est mensonge»: Ces déclarations annonce un retournement de situation avec les patriciens
    + Bilan:
        - Caligula donne une explication philosophique et tragique à son comportement.
        - C'est grâce à Hélicon que le spectateur a accès à la psychologie du personnage.
        - Métamorphose qui révèle un caractère tyrannique
- **III - Relation entre Caligula et Hélicon**
    + a) Diplomatie d'Hélicon
        1. Posture respectueuse
            - Aucune insolence
            - Semble avoir du mal à communiquer
            - Chacun semble peser ses mots (cf silences, pauses)
            - **Cherche à mettre Caligula à l'aise pour qu'il se confie**
        2. Un discours mesuré et bienveillant
            - Posture d'écoute
            - Évite soigneusement de s'impliquer
        3. Bilan
            + Hélicon sert à rappeler le sens commun
            + Permet au spectateur de mieux comprendre la logique de Caligula
    + b) Statut de Caligula
        1. Posture ambigüe d'Hélicon: sincérité ?
            - À ce stade de la pièce, le spectateur peut se demander si Hélicon n'est pas hypocrite, un lâche qui tremble devant le tyran
            - Se méfie des réactions de Caligula, esquive avec une répartie pleine d'humour
            - Précautions de langage donnent l'imporessions qu'il cherche à appaiser Caligula
        2. Confiance de Caligula
            - Caligula manifeste sa confiance envers Héliocon:
                + Il se confie, s'avoue fatigué
                + Didascalies insistent sur le côté familier du dialogue
                + Se laisse conseiller
                + Demande de l'aide
                + Prie Hélicon de garder le secret
        3. Bilan: à la fin de la scène, un pacte est scellé entre eux.
- **Conclusion**
    + Scène permet d'accéder à la psychologie du personnage
    + Permet de mettre en place la quête de l'impossible de Caligula
    + Comment Caligula va-t-il passer de la pensée aux actes ?
