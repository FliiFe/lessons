---
layout: post
title: "«Parfum exotique», Les Fleurs Du Mal, Charles Baudelaire"
excerpt: "Extrait de la section «Spleen et idéal», des Fleurs du Mal"
categories: francais
tags: fr2
author: attssystem
---

1861 = postromantique -> Art pour Art (Parnasse) -> Sonnet

Extrait de la section *"Spleen et idéal"*, dans <u>Les Fleurs du mal</u> **(oxymore)**

> **Les Fleurs du Mal** = sortir du beau du mal <br>
> -> Idéal esthétique<br>
> -> Quête de Baudelaire

> **Spleen** = Humeur noire secrétée dans la rate <br>
> -> Mélancolie (=tristesse vague OU dépression aigüe)

> **Spleen et Idéal** = sortir de l'enfer, atteindre le paradis <br>
> -> Femme = solution<br>
> -> Ici, Jeanne Duval (métisse = exotique = évasion du réel)

## I - Naissance de la rêverie

-   Rituel amoureux déclenche la rêverie
-   Série de sensations => Phénomène de synesthésie

## II - Rêverie heureuse

-   Liée a la quête de l'idéal
-   Univers marin et lumineux
-   *"ile paresseuse"* => Les habitants
    +   Mythe de l'age d'or / Jardin d'Éden
    +   La nature donne

## III - Symbolique du port

-   Point de départ possible
-   Point de débarquement, protection
-   Isolé mais ouvert sur le monde
    + Même symbolique que l'île
-   L'odeur de la femme sert de transition
    +   Femme disparait
    +   Laisse place a un nouveau monde
-   Présence des sens donc monde matériel
    +   Tous les sens
    +   *"Toucher", "encore fatiguée par la vague marine"* => insinuation sexuelle
-   *"Se mêle dans mon âme"*
    +   Passage à une **quête spirituelle**

## Conclusion

-   Hommage à la Femme aimée
    +   Accès a un monde meilleur
    +   par l'amour matériel ou sexuel
        -   Hors de propos a l'époque = provoquant
-   Monde idéal de Baudelaire
    +   Protégé mais ouvert
    +   Rapport Homme/Nature équilibré: pas d'excès
    +   Libre/Affranchi des normes sociales du XIX
