---
layout: post
title: "«Germinal», Zola, incipit"
excerpt: "Incipit de Germinal, publié en 1885"
author: fliife
category: francais
tags: fr3
---

## Plan

- **I - Les éléments spatio-temporels: cadre réaliste et symbolique**
    + a)  Cadre temporel
        - Aucune datation précise
        - Seules indications sont des indications cycliques («deux heures», «petit matin»)
        - «vent de mars» = référence au titre, germinal, septième mois du calendrier révolutionnaire (mars-avril)
        - Fonction symbolique: mars = mois de l'attente
    + b) Cadre spatial
        - Espace précis: toponyme, et Montsou (lieu plausible mais non-réel)
        - Mélange réel-fiction
        - Complétés par la vision typique du Nord
        - Effet de réel renforcé par «**la** route», comme si le lecteur partagait le même référent
    + c) Création d'une atmosphère
        - Champs lexical de l'obscurité
        - Descritpion cernée par la négation
        - **Atmosphère angoissante**
        - Connotation péjorative du mot «ténèbre» suggère une sorte d'enfer glacé
    + Bilan
        - Environnemment particulièrement hostile et spectaculaire
        - Cadre réaliste
        - Fonction symbolique qui génère l'angoisse chez le lecteur.
- **II - La présentation du personnage**
    + a) Présentation progressive
        - Éveille la curiosité: Qui est cet homme ?
        - Restriction de champs comme si on était en point de vue externe
        - Présence d'un narrateur omniscient: connait l'heure de départ, ...
        - Enssemble qui privilégie le pdv interne dans la suite du texte
    + b) Pesonnage sous le signe de la souffrance
        - Champ lexical
        - Confronté à la violence des éléments: «lanière du vent»
        - Souffrance morale: «nuit sans étoiles»
    + c) Indices sur l'identité du perso
        - Solitude, univers hostile (vêtements qui protègent mal) → pauvreté
        - Classe ouvrière, précarité, prolétaire
        - Personnage **subit** «il ne put résister»
- **III - Arrivée aux Voreux: vision inquiétante**
    + a) Thème du regard
        - «Il ne voyait même pas», aveuglement
        - Procédé de retardement mis en place par le pdv interne
        - Mise en scène qui dramatise la découverte du lieu: contraste noir/rouge
    + b) Vision confuse
        - Ne comprend pas tout de suite: articles indéfinis
        - Univers inquiétant car sans cohérence
        - Lexique de l'apparition disparaissante: «s'enfonçait»
    + c) Reconnaissance des lieux: une fosse
        - Du chaos semble naitre une bête gigantesque. Personnification qui donne une dimension mythique. Impression de gigantisme (respiration grosse et longue)
        - Découverte de la fosse qui fait perdre son sens au cadre inquiétant
