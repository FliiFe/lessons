---
layout: post
title: "Caligula, Acte 4 Scène 14, Camus"
excerpt: "Un dénouement tragique"
categories: francais
author: fliife
tags: fr5
---

## Questions possibles:

> En quoi ce monologue est-il particulièrement tragique ?

## Plan

- **I - Un soliloque pathétique et lyrique**
    + a) Dédoublement pathétique
        1. Dédoublement en paroles
            - Apostrophes à lui-même
            - Didascalies qui montrent la dissociation schizofrénique: prend à témoin le miroir
        2. Dédoublement en gestes
            - Didascalies qui montrent la gestuelle pathétique
            - Émotions (en pleurant, en criant, ...) qui forment une gradation jusqu'au bris du miroir
    + b) Décéption devant un constat d'échec amer
        1. Sentiments négatifs
            - Sentiment d'être incompris: «qu'il est dûr d'avoir raison et de devoir aller jusqu'à
            la consommation
            - Regret: «que ne suis-je à leur place»
            - Peur, x3
            - Dégout de sa lacheté qu'il a toujours reprochée aux autres
        2. Bilan dépréciatif de son passé
            - Il n'a pas la lune
            - Regrets exprimés «si j'avais eu..»
            - Impossible = irréalisable: rappel se sa quête
            - Thème de la culpabilité (des autres) + absence de dieu
        3. Constat d'échec
            - Répétition de rien: nihilisme
            - Reniement de soi «ma liberté n'est pas la bonne », «je n'ai pas pris la voie qu'il fallait»
            - **Prise de conscience de s'être tompé**: sa révolte a échoué → révolte collective de ceux qu'il a opprimés en pensant révéler la vériter
    + c) Lyrisme poétique
        1. Thème de la soif d'absolu, impossible à étancher
        2. Images finales de la douleur
        3. Rythme et musicalité lyriques
            - «Tout a l'air si compliqué»/«Tout es si simple pourtant»
            - «Aux bruits du monde»/«Aux confins de moi même»
- **II - Mort attendue, dramatique, et tragique**
    - a) Mort dramatique et dramatisée
        1. Mort attendue
            - Dans la scène 13
            - «Bruits d'armes», «chuchotements en coulisse», miroir brisé
        2. Mort dramatisée
            - En deux temps
                + Entrée d'Hélicon
                + Hurlement de Caligula qui fait face, mais comme sans surprise
            - Enchainements rapides d'actes de violence extrême, soulignés par les didascalies.
    + b) Mort tragique
        1. Gestuelle symbolique
            - Caligula n'a pas fui, mais semble entre démence et histérie: «fait face avec un rire forcé»
            - Frappé en trois temps symboliques
                1. Par le vieux patricien, dans le dos (lâcheté)
                2. Par Cherea, «en pleine figure»: image du résistant
                3. Tous frappent: action solidaire, punition, châtiment attendu et mérité
        2. Polysémie des paroles finales (plusieurs sens)
            - «À l'hisoire, Caligula, à l'histoire»
                1. Mort historique qui assure qu'il soit ancré dans les mémoires
                2. L'histoire lui donne raison: vous êtes tous coupables, menteurs, lâches, comme moi, nous sommes coupables dès lors que nous naissons
        3. «Je suis encore vivant !»
            - **Caligula représente une face potentielle de chaque spectateur.**
            - Mise en garde contre le **mal qui est en chacun de nous**
        4. Sens général de cette mort/suicide
            - «Caligula consent à mourir pour avoir compris qu’aucun homme ne peut se sauver tout seul  et qu’on ne peut être libre contre les autres hommes»
- **Conclusion**
    + Dénoument qui répond à la convention classique: il règle le sort des protagonistes et met un terme à la conspiration en proposant la mort du tyran ayant chatié
