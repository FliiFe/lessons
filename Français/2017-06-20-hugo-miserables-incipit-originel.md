---
layout: post
title: "Victor Hugo, Les Misérables"
excerpt: "Incipit originel"
categories: francais
author: fliife
tags: fr3
---

L'extrait étudié constituait à l'origine, l'incipit du roman.

## Questions possibles:

> Quelle stratégie l'auteur déploie-t-il pour introduire le personnage de Jean Valjean ?

> Ce début de roman est-il traditionel ?

> Quel point de vue l'auteur privilégie-t-il pour présenter Jean Valjean ?

> Ce début de roman annonce-t-il une écriture réaliste ?

## Plan proposé

> Quelle stratégie l'auteur déploie-t-il pour introduire le personnage de Jean Valjean ?

- **I - Un cadre réaliste**
    + a) Cadre temporel très précis
        - Première phrase donne un repère précis
        - Références à des personnages historiques
        - Nombreux repères chronologiques et événementiels
        - **Bilan: Ancre le roman dans l'Histoire contemporaine, et le rend crédible**
    + b) Lieu réel et précis -> Rend le récit plausible
        - Multiplication des indicateurs spaciaux inscrit le roman dans le registre réaliste
        - Toponymes
        - Aménagement urbain («ancien bourg»)
        - Précision quasi documentaire
    + c) Présentation du héro à travers les habitants
        - Verbes de vision
        - Narrateur qui se range du côté des habitants.
- **II - Portrait de Jean Valjean**
    + a) Jeu des points de vue pour dramatiser l'entrée du personnage
        - Premier paragraphe: Frustration du lecteur, qui voudrait en savoir plus
        - Second paragraphe: Confirme le statut de narrateur témoin (cf incertitude du narrateur)
        - Quatrième paragraphe: Glissement pdv omniscient -> pdv des vilageois
        - 5eme et 6eme paragraphes: Alternance de pdv créé un effet de suspension
        - Bilan: incipit suspensif pour rendre l'entrée moins artificielle
    + b) Personnage décrit sous le signe du mystère
        - Identité sociale du personnage → Saleté, Abscence de tenue
        - Sauvagerie, Bestialité
        - Personnage inquiétant
        - Bilan: Aspect extérieur constitue un obstacle serieux à toute réinsertion sociale
    + c) Horizon de lecture
        - Horizon de lecture réaliste, lecteur veut savoir si le héro pourra sortir de sa condition sociale
        - Mais réalisme contrcarré par parallélisme avec Napoléon
        - Bilan: Le lecteur ne doit pas s'attendre à un réalisme objectif car parallélisme annonce une volonté de
        grossir le trait, de grandir les personnages
- **Conclusion**
    - Lecteur averti de l'importance du personnage par la multiplicité des regards + titre
    - Horizon de lecture 
    - Incipit suspensif qui stimule la curiosité par la dramatisation de l'entrée du personnage
