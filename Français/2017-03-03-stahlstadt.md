---
layout: post
title: "«Les Cinq Cent Millions de la Bégum», Stahlstadt, Jules Vernes"
excerpt: "Descritption d'une ville industrielle contruite par le professeur Schultze"
categories: francais
tags: fr1
author: fliife
---

## Plans proposés

> **Question: Montrez que Jules Vernes décrit un univers particulièrement inquiétant**

### Plan 1

Trois aspects peuvent être traités

- **I - Une cité coupée du monde**
    +   Expression de l'enfermement
        *   Ville fortifiée
        *   Choix de l'emplacement
        *   Protection renforcée
    +   Comparaison négative avec les E.U
        *   Fait de Stahlstadt un modele idéal
- **II - Une cité démesurée**
    +   Expression de la démesure
        *   **Pluriels**
        *   => Expression de la puissance
    +   Concentration du pouvoir par Schultze
        *   Idéologique, intellectuel, politique et financier
        *   Génial mais sans limite
            +   **Danger pour l'humanité**
- **III - La culte du secret**
    +   Expression du secret
        *   Ville mystérieuse
        *   Secret jalousement gardé

### Plan 2

- **I - Mise en place d'un univers oppressant**
    1. Cité démesurée
        + Pas de limite, quantités impossible à mesurer
    2. Lieu éloigné, isolé et fermé
        + Entouré de deserts, rempart de montagnes
    3. Secrets et mystères
        + Alliages mystérieux, secrets chimiques, ...
        + Accès à la ville mystérieux: autorisation ? signée/paraphée par qui ?
- **II - Dénonciation: Portrait terrible d'un savant fou**
    1. Un démiurge (=créateur «divin») magicien
        + Il semble diriger mais aussi produire: Sujet de beaucoups de verbes d'action
        + Facilité presque magique: juxtaposition de phrases courtes
    2. Superiorité de ce "superman" de l'acier
        + Amplifications presques épiques
        + Comparaison avec les concurrents
        + Gradation qu'il couronne (Cf. M.Krupp)
    3. Dénonciation derrière le portrait épique
        + Pas de vie dans sa cité, pas d'humain.
        + Empereur de la pire des contre-utopies
        + Met sa puissance au service de la guerre
        + Valeurs immorales: dominé par le gout de l'argent et du pouvoir

## Recherche

-   Qui ?
    +   Jules Vernes
-   Où ?
    +   France
-   Quoi ? 
    +   Contre-utopie
    +   Article sur Stahlstadt, "La cité de l'acier"
        *   Description d’une ville industrielle conçue 
            par le professeur Schultze en compétition avec
            le professeur Sarrazin, un français qui construit
            Franceville => Reportage/Recit de voyage
-   Quand ?
    +   Après la guerre franco-prussienne
-   Pourquoi ?
    +   Commandé par Etzel (littérature jeunesse)
        *   Éducation
        *   Patriotisme
    +   Oeuvre de divertissement
    +   Fonction argumentative
    +   Fonction Instructive
        *   Critique du modèle Allemand
-   Comment ?
    +   Description d'un univers angoissant
    +   Expression de la démesure
        *   **Pluriels**
        *   => Expression de la puissance
    +   Concentration du pouvoir par Schultze
        *   Idéologique, intellectuel, politique et financier
        *   Mythe du savant fou
        *   Projet peu éthique
        *   Génial mais sans limite
            +   **Danger pour l'humanité**
    +   Registre fantastique
    +   Expression du secret
        *   Ville mystérieuse
        *   Secret jalousement gardé
    +   Expression de l'enfermement
        *   Ville fortifiée
        *   Choix de l'emplacement
        *   Protection renforcée
    +   Narrateur ignorant, mais pas neutre
        *   Adresse directe au lecteur
    +   Comparaison négative avec les E.U
        *   Fait de Stahlstadt un modele idéal
