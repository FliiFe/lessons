---
layout: post
title: "«L'Eldorado», Candide, Voltaire"
excerpt: "Arrivée au palais"
categories: francais
tags: fr1
author: fliife
---

## Plan proposé

- **I - Un monde inversé**
    1. Garde composée de femmes alors que traditionnellement, il s'agit d'une tache masculine
    2. Accessibilité du roi, alors que Candide induit une culture dans laquelle pouvoir = violence et humiliation
        - Satire du pouvoir: l'absurdité des rituels est remplacée par des rites simples et chaleureux
    3. Pas d'institutions judiciaires/prison
        - Absence de système repressif très présent en monarchie traditionnelle
- **II - Monde idéal et merveilleux**
    + a) Luxe et richesse: matériau incunnu supérieur à l'or, «supériorité prodigieuse»
    + b) Gigantisme: immensité des portails, édifices «jusqu'aux nues»
    + c) Éléments urbains soignés. Thème de l'abondance (pluriels, «toutes pleines»)
    + Bilan: 
        - Caractéristiques de l'Utopie
            + Nouvelle organisation sociale
        - Caractéristiques du conte merveilleux
            + Exagération
- **III - Sens de l'Utopie**
    + a) Éloge du bien-être
        - Douceur/vêtement/parfum/abondance/...
    + b) Image du monarque
        - Proche de ses sujets, accueillant
    + c) Une société ouverte
        - Fondée sur la raison
        - Tournée vers la science contre l'obscurantisme
        - Favorise les Arts
    + d) Société idéale
        - Pas de délinquance ni de criminalité
        - Peut être pas de notion de mal
- **Conclusion**
    + Candide = porte parole de Voltaire.
    + Moyen pour voltaire de transcrire ses idéaux politiques, sociaux et religieux.
        - Société qui accorde de l'importance à la connaissance, au progrès
