---
layout: post
title: "Pierre Boulle, La Planète des singes, Ouverture, 1963"
excerpt: "Ouverture du roman"
categories: francais
author: fliife
tags: fr4
---

## Plan possible

- **I - Recherche du dépaysement**
    + Première phrase surprend car les personnages ne sont pas des explorateurs, et nous sommes loin du tourisme spacial banal.
    + Nous enmène dans un ailleurs inaccessible: cosmos, espace, ...
    + Hors du système solaire
- **II - Personnages à la fois étranges et familiers**
    + a) Familiers
        - Descriptions qui porte sur l'attitude, pas le physique
        - Prises de risque font penser qu'ils sont jeunes
        - Riches
    + b) Étranges
        - Aventuriers: thème du voyage
        - Système stellaire différent
    + c) Personnages qui entrainent la sympathie
        - ≠ cliché de l'extraterrestre envahisseur
    + Bilan: C'est surtout l'étrangeté du cadre qui fait la spécificité des personnages
- **III - Cadre futuriste**
    + a) Nouvelles technologies
        - Éléments spécifiques au genre de la SF
        - 1ère phrase s'ouvre sur une prouesse technologique pour les années 60: le vol spacial habité
        - Description du fonctionnement du vaisseau anticipe sur l'exploitation de nouvelles énergies (ici lumineuse)
        - Termes techniques qui crédibilisent le discours
    + b) Mode de navigation poétique
        - Fait rêver
        - Ne s'appuie pas sur la fusée
        - Navigation à voile permet de faire le lien avec navigation marine (dont le champ lexical est très présent)
        - Raproche le personnage de notre univers, car il connais nos modes de locomotion.
        - Anonce d'un roman d'aventure avec les deux champs lexicaux et la mention de tempêtes et orages
- **Conclusion**
    + Ouverture qui ressemble à un incipit traditionnel
    + En revanche, fausse piste: personnages qui ne sont pas au centre de l'histoire.
    + Préparation du coup de théâtre de l'épilogue
