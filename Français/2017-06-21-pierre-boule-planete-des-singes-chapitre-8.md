---
layout: post
title: "Pierre Boulle, La Planète des singes, Troisième livre, Chapitre 8, 1963"
excerpt: "Les origines"
categories: francais
author: fliife
tags: fr4
---

Le roman: Apologue ? Conte philosophique ?

## Plan possible

- **I - Effet de réel à partir de l'illusion d'un témoignage au discours direct**
    + a) La science fiction
        - Futur éloigné, permet à l'auteur d'avertir sur les dangers qui menacent l'humanité
        - Passage s'inscrit d'emblée dans la SF: nous assistons à un «miracle» scientifique
    + b) Stratégie narrative: témoignage d'un passé lointain
        - Analepse. Original car ne s'appuie que sur des bribes de discours direct
        - Nombreuses ellipses narratives, chaque prise de parole correspond à une étape
    + c) Témoignage de la décadence des hommes et de l'ascension des singes
    + Bilan: Les trois voix qui témoignent expriment de l'angoisse et conrtibuent à renfocer la
    tension dramatique → évite un long exposé didactique
- **II - L'extrait retrace les principales étapes de l'évolution des rappors homme-singes**
    + a) Les principales étapes
        1. Lhomme utilise le singe comme domestique
        2. Le comportement des singes change: ils ne craignent plus leur maitres.
        3. Ils refusent d'obéir
        4. Les singes deviennent indépendants
        5. Les rôles se renversent, les hommes servent les singes
        6. Les singes chassent les hommes de leurs demeures
    + b) Explication de cette évolution
        - Déchéance des singes vs ascension des singes (activité intellectuelle, paresse cérébrale)
    + c) Actions humaines attribuées aux singes
        - Singes ont des comportement humains avant meme de parler: il chuchotent, ils rient
        - Ils parlent
        - Ils développent leurs activités intellectuelles: verbes de pensée
        - Leur comportement évolue et deviens plus humain.
    + d) La construction grammaticale des phrases traduit la prise de pouvoir des singes
        - Le singe est sujet de l'action: il agit.
        - Homme sujet des verbes modaux (oser, devoir, essayer)
    + Bilan: Processus qui aboutit à la prise de pouvoir des singes, déshumanisation des hommes.
        - L'humanité se cultive, se maintient, et se conquérit. La paresse mène à la perte de celle-ci
        - Une culture fondée sur la violence appelle à une réponse violente
- **III - Un récit de SF**
    + a) Caractéristiques de la SF dans cet extrait
        - Situation et évenement apparetenant au futur.
        - Extrapolation des données contemporaines (Cf paresse intellectuelle qui menace les sociétés de conso)
    + b) Destin présenté comme effrayant
        - Privation totale de liberté pour l'homme
    + c) Mise en garde addressée aux hommes
        - Homme apparait comme entièrement responsable
        - Auteur dénonce le danger qui guette une société dont les valeurs dominantes seraient la consommation et le plaisir.
- **Conclusion**
    + La suprématie de l'homme sur les autres espèces pourrait être remise en question
    + Questionne sur la violence de certains rapports de force
    + Insiste sur l'importance de la culture et du langage articulé
