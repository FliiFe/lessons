---
layout: post
title: "«Les Conquérants», Les Trophées, Heredia"
excerpt: "La découverte du nouveau monde"
categories: francais
tags: fr2
author: fliife
---

## Questions possibles

> Comment le poète nous montre-t-il l’évolution psychologique des conquérants ?

> Comment la vision des conquérants évolue-t-elle au fur et à mesure de leur voyage ?

> Comment l’auteur rend-il hommage à son ancêtre Don Pedro ?

> Comment l’auteur nous montre-t-il l’état d’esprit « des conquérants » ?

> Quelle sorte de voyage l’auteur évoque-t-il dans ce poème ?

> Comment l’auteur évoque-t-il dans son poème la découverte du Nouveau Monde ?

## Exemple de plan

- **I - Le voyage**
    + a) concret, exotique
        - de Palos de Moguer à Cipango
        - Champs lexical du voyage: routiers, vol, partaient, allaient
    + b) imaginaire, intérieur, constitué par le rêve
        - évocation du «mirage doré»
        - champs lexical de l'illusion
- **II - Violence/Orgueil**
    + a) Thème de la conquête
        - Veulent la gloire, la reconnaissance sociale, la richesse
        - Leur ambition repose sur la maîtrise du combat
        - Champs lexical de la guerre: capitaines, gerfault
    + b) Image héroïque remise en cause
        - Métaphore du gerfaut: figure de prédateur
