---
layout: post
title: "Caligula, Acte 1 Scène 1, Camus"
excerpt: "L'exposition, interrogations"
categories: francais
author: fliife
tags: fr5
---

## Questions possibles:

> La première scène peut-elle constituer seule l'exposition de la pièce ?

> Comment nous présente-t-on le personnage éponyme

## Plan proposé

- **I - Des éléments d'intrigue plutot imprécis**
    + a) Répétition de «rien»
        - Mot clé de la scène 1
        - Personnages dans l'ignorance
    + b) Atmosphère dominante: inquiétude
        - «Je n'aime pas cela», «*Toujours nerveux*», «Du calme»
        - Répliques montrent que les personnages sont confrontés à une situation inhabituelle
        - Évolution peut être en leur défaveur
    + c) Éléments d'intrigue
        - Cause de l'inquiétude ?
        - Fuite de l'empereur ? 
            + Personne ne sait où il est, ce qu'il fait
            + On sait qu'il est malheureux, et que ça pourrait poser problème
        - Intervention d'Hélicon qui trouble le diagnostic des patriciens
            + Remet en cause la thèse de l'amour
            + Remet en cause l'impact de la mort de Drusilla
            + Modalisateurs de doute
    + Bilan
        - Première scène intrigante
        - Rend le personnage de Caligula mystérieux
        - Ne nous apprend pas grand chose sur la suite
        - Spectateur contraint, comme les patriciens, d'attendre Caligula
        - On s'attend quand même à une transformation psychologique
- **II - Portrait des patricien**
    + a) Personnages sans individualité caractérisé par leur classe sociale
        - Dydascalies n'indiquent pas leur noms, juste leur âge
        - Pensée unique basée sur des clichés («une de perdue, dix de retrouvées», «la raison 
        d'État ne peut admettre un inceste qui prend l'allure d'une tragédie»)
    + b) Personnages manipulateurs
        - Le dialogue montre qu'ils profitent de la naiveté et de la jenesse pour manipuler
        - Hélicon montre que Caligula, pour la première fois échappe à leur contrôle → déstabilisés
        - Hélicon semble mieux connaitre Caligula, il est individualisé (on connais son nom, comme Cherea)
    + c) Patriciens préocupés par le pouvoir plus que l'affectif
        - Réflexions du premier patriciens sur l'amour et l'inceste
        - Ce n'est pas Caligula qui inquiète, c'est l'empire.
    + Bilan
        - Portrait négatif des patriciens, ce qui empèche de s'identifier complètement à leur inquiétude
        - Portrait des patriciens:
            + Caste qui cherche à s'arroger le pouvoir
            + S'arrangent de la faiblesse de Caligula
        - Portrait de Caligula:
            + Manipulable
            + Victime de son entourage
- **III - Portrait de Caligula mystérieux**
    + a) Mystère qui réside dans l'abscence du personnage
        - Présenté uniquement à travers le point de vue des personnages.
        - Considéré comme un enfant
        - Cherea et le premier patricien s'inquiètent de l'inceste, avec une maxime: ce sont les patriciens qui décident de la raison d'État
        - Craignent que Caligula fragilise le pouvoir avec un scandale, ce qui remettrait en cause leur propre pouvoir
        - L'inquiétude des patriciens montre que **quelque chose est en train de changer**: ils ne sont plus sûrs de controller Caligula.
    + b) Autre regard sur Caligula: celui d'Hélicon
        - Moins soumis
        - PDV propre à déstabiliser davantage les patriciens
        - Remet en question l'interprétation du chagrin d'amour avec modalisateurs d'incertitude
        - Référence à la théorie des humeurs (Spleen de baudelaire): foie provoque la mélancolie
        - Remet en cause l'impact de Drusilla
        - Suggère un malheur plus existentiel («ne sait peut etre pas pourquoi»)
    + Bilan: Le point de vue d'Hélicon évoque un personnage moins enfantin à la psychologie plus complexe, et donc plus inquiétante pour les patriciens
- **Conclusion**
    - Spectateur en état d'attente: possède le contexte mais Caligula n'est pas assez éclairé
    - Actien semble dépasser l'état d'esprit: on hésite entre plusieurs hypothèse, *l'intrigue n'est pas fixée*
