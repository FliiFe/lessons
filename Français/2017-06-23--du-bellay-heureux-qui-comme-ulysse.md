---
layout: post
title: "«Heureux qui comme Ulysse», Du Bellay, 1558"
excerpt: "Extrait du reccueil Les regrets"
categories: francais
tags: fr2
author: fliife
---

# Plan proposé

- **I - Expression sur un ton élégiaque, douleur de l'exil**
    + a) Éloge du voyage, dont le poète semble être exclu
        - Ouverture à la troisième personne, pronom anonyme
        - Évocation d'Ulysse étrange, car il est plutot un héro du retour
        - Deuxième partie de la première strophe constitue une éloge du retour
        - Expression du regret, car le poète en est toujours exclus
    + b) Comparaison entre dimension souhaitable et expérience personnelle du poète
        - Marques explicite de l'énonciation à la première personne
        - Ponctuation expressive qui renforce le lyrisme
        - Hélas → Mélancolie, Nostalgie
    + c) Nostalgie qui domine l'ensemble du sonnet
        - Irruption de la première personne tranche avec la première strophe: douleur de l'exil
        - 2eme strophe = question, qui traduit l'angoisse et l'incertitude
- **II - Désenchantement**
    + a) Confrontation qui occupe les deux tercets
        - Anaphore «plus que» → préférence pour son pays natal
        - Renforcée par les lieux séparés par la césure à l'hémistiche
    + b) Douceur angevine vs Dureté de Rome
        - Prestige de rome vs dureté et arrogance de rome («marbres durs», «fronts audacieux»)
        - Petit Liré vs Le Tibre, air marin vs douveur angevine... Témoignent d'une autre mentalité
        - Ici, voyage de la mémoire, d'écriture, pas seulement sentiment d'exil, mais aussi refuge contre la mentalité de Rome à laquelle il n'adhère pas.
- *(Facultatif)* **III - Poète déçu, mais qui a gagné en réflexion, et l'oeuvre s'inscrit dans un projet humaniste**
    + a) Poète se nourris de son expérience perso mais aussi sa relation avec la culture Antique
        - Invite à la redécouverte de la culture Antique
        - Poète ne pense pas Rome comme une ville, mais comme l'emblème d'une culture.
    + b) Voyage = temps d'apprentissage
        - Voyager, c'est gagner en «age», en «raison», etn «usage». Le voyage devrait permettre la métamorphose positive de l'individu.
    + c) Deux tercets = hommage à un art de vivre et à une culture, éloge de la France face à l'Italie
- **Conclusion**
    + Dénonciation du piège de la nostalgie: idéaliser ce qu'on a perdu.
