---
layout: post
title: "Pierre Boulle, La Planète des singes, Chapitre 9, 1963"
excerpt: "La chasse à l'homme"
categories: francais
author: fliife
tags: fr4
---

- **Signes d'humanité**
    + Malgré la panique, Ulysse va vers les coups de feux, parce que les bruits lui sont familiers.
    + Marques d'énonciation montrent la prise en compte d'un ou plusieurs destinataires identifiés comme terriens
        - «nous» communautaire
        - Références à l'univers terrien: tailleur parisien, chemise à carreaux de nos sportifs
- **Inversion des rôles**
    + Ce sont les singes qui chassent, pas les hommes.
    + Cruauté fait partie des normes humaines
