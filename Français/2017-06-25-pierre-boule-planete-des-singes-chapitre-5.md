---
layout: post
title: "Pierre Boulle, La Planète des singes, Chapitre 5, 1963"
excerpt: "Rencontre avec Nova"
categories: francais
author: fliife
tags: fr4
---

> Quel est le portrait donné à Nova ?

> Comment Nova est-elle décrite ?

- **Portrait ambivalent**
    1. Un portrait élogieux
        + Nova est décrite comme idéale.
        + Correspond à une humaine: «femme […] jeune fille […] de race blanche»
    2. Un personnage hors-norme
        + Ulysse subjugué: place Nova dans l'illusion, l'anormalité, voire la **montruosité**
        + Comparaison à une déesse: **Non-humaine**
    3. Une description en deux temps
        1. Subjective: Subjugation d'Ulysse
        2. Plus objective et scientifique
            + Verbe «distinguer»
            + Vision plus analytique: connecteurs temporels «ensuite», «enfin»
- **Personnage auquel on ne peut pas s'identifier**
    + Malaise d'Ulysse, qui ne se reconnait pas dans la posture ou les réactions de la femme
        - Champ lexical de l'étrangeté
        - Regard d'Ulysse conditionné par ses attentes: «dans un monde si éloigné du notre»
    + Expression de la femme
        - Comparée à celle de la folie
        - Hypothèse de la folie immédiatement écartée, pour laisser place à une comparaison avec l'animal
            + Champ lexical de l'animalité
            + Animalité renforcée par le cadre naturel
            + La femme réagit comme un animal sauvage
- **Conclusion**
    + Métamorphose du regard d'Ulysse
    + Il en faut peu pour retirer une personne de l'humanité
    + Sentiment de pudeur considéré comme universel
