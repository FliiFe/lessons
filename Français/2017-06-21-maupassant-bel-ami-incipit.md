---
layout: post
title: "Maupassant, Bel Ami, incipit"
excerpt: "Axes d'étude possibles: Le portrait, l'incipit de roman, le héros de roman"
categories: francais
author: fliife
tags: fr3
---

Axes d'étude possibles: Le portrait, l'incipit de roman, le héros de roman

## Questions possibles

> Cet invipit remplit-il bien son rôle ?

> Quel portrait du héros cet incipit propose-t-il ?

> En quoi cet incipit de Bel-Ami est-il annonciateur du reste du roman ?

> Georges Duroy, un anti-héros ?

> En quoi cet incipit annonce-t-il un roman réaliste ?

## Plan proposé

- **I - Un incipit traditionnel**
    + **a) Le cadre spacio-temporel**
        - Lieu précis, toponymes
        - Date précise
        - in medias res, après un repas
    + **b) Présentation du perosnnage principal**
        - Protagoniste apparait dès la première phrase
        - Jeune homme, pauvre, vie modeste
        - Attitude conquérante
        - Changements de points de vues: externe, puis interne («il refléchit que») puis externe
- **II - Portrait à double tranchant**
    + **a) Portrait physique flatteur**
        - «joli garcon», regard des femmes, ...
        - Personnage conquérant, fière allure, prestance malgré sa pauvreté
        - Personnage superficiel
    + **b) Portrait en mouvement**
        - Narrateur montre Duroy en mouvement → personnage définit par ce qu'il fait, pas ce qu'il est
        - Verbes d'action, mouvements rapides
    + **c) Portrait psychologique ambigu**
        - S'apparente à un voyou, chapeau incliné
        - Portrait critique: vaniteux, séducteur, aggressif sans raison.
        - Son attitude conquérante devient ridicule
        - Personnage peu responsable
- **III - Un incipit proleptique**
    + **a) l'argent**
        - Thème évident de l'incipit
        - Calculs monétaires
        - Pauvretée sans cesse rappelée
    + **b) les femmes et la séduction**
        - Jeune et fringant
        - Rapport aux femmes: chasse
        - Désir de séduction = désir de richesse
    + **c) Arrivisme de Georges Duroy**
        - Fait tout pour se faire remarquer
        - «quelqu'un, les passants, ... la ville toute entière» → ambition démesurée, mégalomanie
- **Conclusion**
    + Remplit la fonction de mise en contexte
    + Représentatif du roman naturaliste: on suit duroy jusqu'au triomphe social et économique,
    en abordant les thèmes majeurs: l'argent, la séduction et l'arrivisme
