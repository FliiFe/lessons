---
layout: post
title: "France-ville, Les Cinq Cents Millions de la Bégum, Jules Vernes"
excerpt: "Jules Vernes a publié cette oeuvre sous la commande de son éditeur Hetzel"
categories: francais
author: geoffrey
tags: fr1
---

<table>
<colgroup>
    <col style="width:100px">
  </colgroup>
<tbody>
<tr>
<td>Qui ?</td>
<td>Jule Verne a publié cette œuvre sous la commande de son éditeur Hetzel.</td>
<td>Célèbre auteur français connu pour ses nombreux romans destinés à la jeunesse.</td>
</tr>
<tr>
<td>Quoi ?</td>
<td>Un article de presse d’un journaliste allemand faisant l’éloge de France-ville une utopie construite par le professeur Sarrazin (célèbre professeur français)</td>
<td>Cette utopie est construite selon un modèle scientifique qui se caractérise par une architecture parfaitement géométrique et identique ainsi qu’une sécurité sanitaire extrêmement importante.</td>
</tr>
<tr>
<td>Quand ?</td>
<td>En 1879.</td>
<td>Après la guerre Franco-prussienne de 1870. Après une d’épidémie de la tuberculose. Jule Verne met cette cité en paralléle avec une autre appelée Sthalshadt, une citée allemande construite par le professeur Schulze qui parait comme une dystopie.</td>
</tr>
<tr>
<td>Où ?</td>
<td>Ce roman est publié en France.</td>
<td>Dans le but de proposer une cité idéale et de faire prendre conscience au citoyens l’importance de l’hygiène suite à la forte épidémie de la tuberculose.</td>
</tr>
</tbody>
</table>

## Les utopies rêve ou cauchemar ?

|                                                    Rêve                                                   |                                                                  Cauchemar                                                                 |
|:---------------------------------------------------------------------------------------------------------:+:------------------------------------------------------------------------------------------------------------------------------------------:|
|                       Description élogieuse de la ville par le journaliste allemand.                      |                              Une architecture trop géométrique et partout identique qui peut sembler monotone.                             |
|              Un plan de ville simple et régulier de manière à se prêter a tout développement.             |                            Sélection des habitants (seul ceux ayant de bonnes référence peuvent s’y installer).                            |
| De nombreux espaces pour le divertissement des habitants (jardins publiques, gymnase, bibliothèque…ect ). |                                                Les existences oisives n’y sont pas tolérées.                                               |
|                    Equilibre entre le corps et l’esprit dans l’éducation des citoyens.                    |                          Les malades sont isolés chez eux au lieux d’être emmené dans un véritable lieux de soin.                          |
|                     L’hygiène a la principale priorité de la ville et des habitants...                    | ...au point de conditionner les habitants (pour eux une simple taches représente un réel déshonneur) qui ont un sens de l’hygiène exagéré. |
|                       L’art et les sciences ont une places importante dans la ville.                      |                                                                                                                                            |
