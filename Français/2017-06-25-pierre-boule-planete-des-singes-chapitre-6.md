---
layout: post
title: "Pierre Boulle, La Planète des singes, Chapitre 6, 1963"
excerpt: "Scène du jeu dans le lac"
categories: francais
author: fliife
tags: fr4
---

> La population découverte est-elle humaine ?

- **Une population à l'apparence humaine**
    + Termes utilisés pour les désigner/caractériser: «un homme», «échantillon d'humanité», «semblable aux hommes de la terre»
    + Physiquement, ressemble à la population terrienne
    + Paratexte signale la présence de villes
- **Une population désignée comme non-humaine**
    + Termes qui les désignent comme en dehors de l'humanité: «créatures étranges» «êtres»
    + Termes qui les classent dans les animaux: «barbotant» «petits cris»
    + Nus, ne connaissent pas la notion de pudeur, ne sourient pas, ne parlent pas
    + Semblent ne pas penser, n'ont aucune reflexivité
    + Pour Ulysse, dénuée de raison/ame. → Poupées vides, mécaniques.
    + Hésitation entre homme et animal: Ulysse oscille entre les deux.
- **Conclusion**
    + Humanité ne tiens pas seulement aux caractéristiques morphologiques
    + Narrateur insiste sur la ressemblence avec l'humain, mais ne sais pas identifier l'espèce.
