---
layout: post
title: "Caligula, Acte 2 Scène 10, Camus"
excerpt: "Une démonstration de pouvoir"
categories: francais
author: fliife
tags: fr5
---

## Questions possibles:

> Comment Caligula est-il représenté ?

> Quelle vision de l'homme et du monde Camus met-il en place dans cet
> extrait ?

> Montrez que Caligula incarne la démesure

## Question d'entretien

> Quels sont les registres présents dans ce texte ?

## Plans possibles

La scène peut être coupée en deux: scène du "Trésor public"/scène du "meurtre de Mereia".
Il faut donc adapter le plan.

_Ex: **Trésor public**: Comment cet extrait souligne-t-il la toute puissance de Caligula ?_

- I - **Caligula et la maîtrise du discours**
- II - **Caligula et la maîtrise des valeurs**: Inversion des valeurs morales/dévoiement de la liberté
- III - **Face à Caligula**: Impuissance et collaboration

_Ex: **Meurtre de Mereia**: Comment ce passage met-il en scène une parodie de procès ?_

- I - La maîtrise du discours: la victime désigée
    + a) Le prédateur et sa proie: humiliation de Méréia (rappel du début de la scène).
    + b) Une parodie de procès: un discourd d'une logique implacable/accumulation des chefs d'accusation
      (Cf. [I-b)](#b-le-discours-de-la-tyrannie-un-ton-péremptoire-qui-impressionne-et-entrave-la-contestation) et [II-c)](#c-une-parodie-de-justice-enfin-la-notion-de-justice-est-constamment-atteinte))
- II - Une démonstration de force (Cf. [II-c)](#c-une-parodie-de-justice-enfin-la-notion-de-justice-est-constamment-atteinte) et [II-d)](#d-le-mépris-de-la-vie-humaine))
    + a) Solitude et faiblesse de Mereia: une victime réduite peu à peu au silence
    + b) Violence extreme du tyran: une scène **spectaculaire**
    + c) Mépris de la vie humaine

_Ex: Caligula est-il un monstre ?_

- I - Détournement des valeurs morales
- II - Institutionnalisation de l'injustice et de la violence
- II - Autres persos ? Collaborent et se rendent ± indirectement complices des crimes

## Bilan

Caligula est entré dans un processus déshumanisant pour tous. En même temps, Camus nous fait
pénetrer dans l'obscurité de l'âme humaine: *Caligula est en chacun de nous*

## Analyse

### I - Caligula impose d’abord son autorité par la maîtrise du discours.

Caligula semble vouloir entretenir un de dialogue avec ses conseillers mais ce
n’est qu’un faux semblant. Au contraire, Caligula démontre d’abord sa
puissance à travers la maîtrise du discours.

#### a) Caligula garde l’initiative de la parole. Il lance et maitrise les sujets de conversation :

- Il impose ainsi la tenue la tenue d’un conseil  qui n’est qu’un faux semblant :
Le thème de la conversation a été lancé à la fin de la scène 9, c’est un thème
provocateur puisqu’il s’agit des « maisons publiques » de Caligula. Quand il le
décide, le centre d’intérêt change. Mereia va en faire les frais : « Que bois-tu
Mereia ? ». Il semble l’avoir choisi comme souffre-douleur.
- Il maîtrise les échanges : L’empereur évoque les problèmes financiers et semble
demander conseil par l’énonciation d’un bilan négatif : « Cherea : …Qu’est-ce qui ne
va pas ?/ Caligula : …les recettes ne sont pas bonnes. » p89. Mais on s’aperçoit
rapidement que la solution est déjà trouvée et qu’il ne demande qu’une présence
de façade, une approbation équivalant à une posture de soumission : ils semblent
en fait convoqués pour être spectateurs de la tyrannie, de la toute-puissance de
Caligula.

Caligula maîtrise en effet la répartition de la parole et le contenu du
discours des patriciens en sa présence : Il ridiculise Mereia et fait une allusion
douteuse sur son impuissance p89 et lui dénie le droit à la parole : « je ne te
demande pas ton avis. ». Lorsque Cherea prend la parole, il le renvoie au discours
de Caesonia qui va expliquer son plan : « Caligula crée une nouvelle
décoration. »p90. Hélicon et Caesonia se font alors porte-parole du tyran:
implicitement Caligula refuse tout véritable dialogue et le montre par la fermeture
de sa posture : « il s’étend et ferme les yeux ». Il n’est donc pas question de
débattre.

#### b) Le discours de la tyrannie : un ton péremptoire qui impressionne et entrave la contestation.

Le discours de Caligula est direct et péremptoire, dénué de toute hésitation.
Ses phrases sont sans ambiguïté, émaillées de modalisateurs de certitude :
«Naturellement voyons. Mais il faut nous rattraper sur les chiffres d’affaires.»p90.
Caligula évite les phrases complexes et s’exprime sèchement par des
phrases simples et courtes (avec le plus souvent un seul verbe par phrase),
généralement affirmatives : « Tu me suspectes. En quelque sorte, tu te défies
de moi. » p92. Il s’exprime également par des questions rhétoriques (A
Mereia : « Ainsi, tu as peur d’être empoisonné ? »p92 : Caligula connaît ou croit
connaître déjà la réponse.

Il cherche à contrôler jusqu’au discours. Ainsi, lorsqu’il accuse Mereia, l’injonction
« Réponds-moi  p92»incite Mereia à avouer et ne sert pas la vérité mais la vérité
de Caligula. La réponse attendue n’est autre qu’une posture de soumission encore
une fois ; et c’est Caligula qui produit lui-même l’aveu.  

#### c) Un discours qui s’appuie sur une logique de façade

En effet, la pseudo-reconstitution de la pensée de Mereia, inscrit le discours de
Caligula dans un cadre logique de bon sens : phénomène déjà observé à la
scène 9 lorsqu’Hélicon récite le traité de l’exécution sous la forme d’un
raisonnement proche du syllogisme p88.

Même si ses décisions sont folles (cf. scène 9 « je dis qu’il y aura famine
demain. »), elles s’appuient toujours sur des éléments rhétoriques formels qui
donne au discours de Caligula l’allure de la rationalité.

C’est ce que montre le faux dialogue d’aveu entre Caligula et Mereia :

- Notamment la didascalie « Réponds-moi. (Mathématique.) Si tu prends un
contrepoison, tu me prêtes par conséquent l’intention de t’empoisonner. »
- La présence de connecteurs logiques qui indiquent les liens de cause à effet :
connecteurs explicites comme « par conséquent »p92, « parce que » p93.
ou connecteurs implicites : « Et dès l’instant où tu crois que j’ai pris la décision
de t’empoisonner, (connecteur implicite : la virgule qui remplace « alors ») tu
fais ce qu’il faut pour t’opposer à cette volonté. » p93.
- L’alternative : « Ou…ou bien » qui enferme définitivement Mereia dans la
culpabilité sans lui laisser la moindre chance de se disculper.
Le soupçon devient affirmation. Le tyran monopolise la parole et décide lui-
même de la pensée du patricien et de son nécessaire statut criminel dans une
logique implacable de cause à effet.

#### Bilan

Ainsi, Caligula s’impose à l’assemblée des Patriciens et désamorce
toute tentative de débat. On peut dire qu’il n’y a pas de véritable dialogue.
Nous sommes dans le discours de la tyrannie.

### II - La manipulation des valeurs comme expression de la toute-puissance.

La maîtrise du discours n’est pas le seul signe de la puissance du tyran.
Caligula aussi affirme son pouvoir par sa capacité à pouvoir manipuler les
valeurs de la société en place, ce qui souligne sa volonté de changer l’ordre des
choses…surtout pour le pire. En effet, pour mettre en avant les valeurs de
vérité et de liberté, valeurs effectivement importantes, Caligula est prêt à
détruire d’autres valeurs comme la vie, l’amour ou la justice.

#### a) L’inversion des valeurs morales : par-delà le Bien et le Mal

Dans la première partie de la scène, les valeurs morales sont inversées : sous
prétexte de renflouer les caisses de l’Etat., Caligula veut forcer les sujets à aller
dans des maisons closes. La décision absurde de taxer le vice peut paraître
comique : elle joue aussi sur l’aspect cru et prosaïque de la sexualité, qui
représente une des formes du comique. Cependant, accepter d’en discuter, c’est
déjà en accepter le principe. Ainsi, cette décision ne paraît pas choquer pas les
patriciens qui ne protestent pas. Leur silence tout comme les encouragements de
Cherea (« Lumineux. » – « Bravo.»). peuvent donc être interprétés comme une
adhésion.

Hélicon rend explicite Le retournement des valeurs morales: « CHEREA : Bravo.
Le Trésor public est aujourd’hui renfloué. HÉLICON : Et toujours de façon très
morale, remarquez-le bien. Il vaut mieux, après tout, taxer le vice que rançonner la
vertu comme on le fait dans les sociétés républicaines. » Cet éloge paradoxal de la
dictature est ironique : il montre la soumission extrême des patriciens à
l’empereur. Bilan : Caligula pousse le principe du gouvernement qu’il dénonce
(« Gouverner , c’est voler, tout le monde sait ça. » I, VIII p56) jusqu’au bout, il le
généralise et au lieu de rendre ses sujets heureux, il les fait vivre dans la terreur et
l’humiliation. D’autre part, Camus propose ici une vision pessimiste de la nature
humaine : la résistance n’est pas une évidence. L’homme est prêt à dire n’importe
quoi tant que cela lui permet de garder la vie sauve. D’autres valeurs vont être mises
à mal dans cette scène.

#### b) Une parodie de liberté ? Un défi lancé par Caligula…

La démesure de Caligula l’amène à fausser certaines valeurs. La liberté qu’il
défend est d’abord « sa » liberté : elle n’a rien d’universel et s’apparente
davantage au caprice. Lorsqu’il décide de la sanction des « désobéissants » qui
délaisseraient ses maisons closes p91, l’alternative proposée ne laisse
finalement pas le choix aux citoyens qui doivent s’exiler, ce qui peut équivaloir
à une mort sociale.

Ce choix n’est qu’un faux semblant, à part pour quelques héros comme
Socrate qui choisissent le suicide, le reste de l’humanité ordinaire choisira
l’exil. L’héroïsme, l’idéalisme jusqu’à la mort n’est pas courant…On peut donc
considérer qu’il s’agit d’une provocation de la part de Caligula. La générosité de
l’alternative n’est qu’une démonstration de cruauté et de mépris.

#### c) Une parodie de justice : Enfin, la notion de Justice est constamment atteinte.

1. La parodie de procès que subit Mereia montre que la liberté du tyran prime sur la justice. Dès le début de la scène, on voit que Caligula se plaît à l’humilier :
   « Mereia tu viens de perdre l’occasion de te taire »./ « je ne te demande pas ton
   avis » p89 .. La posture de Caligula indiquée par la didascalie en fait une proie
   potentielle pour ce prédateur : « Caligula ouvre les yeux à demi et regarde le vieux
   Mereia… ». cette posture rappelle davantage celle de l’animal.

2. Caligula invente le crime et inverse les rôles : son accusation révèle que la
   faiblesse de Mereia a réveillé son instinct destructeur et annonce la mort du
   patricien. Cependant, Caligula se présente comme victime du crime de lèse-
   majesté : il accumule méthodiquement les chefs d’accusation :
    - Ou bien…et (= par conséquent) tu me suspectes injustement… ». La gradation
    insiste sur l’importance de la victime et donc sur la gravité du crime : « me »/
    « moi »/ « ton empereur » tout en exprimant l’indignation./
    - Ou bien je le voulais, et toi, insecte, tu t’opposes à mes projets. Là encore le
    redoublement du pronom personnel et du pronom tonique « toi » contraste avec la
    première personne. L’insulte « insecte » montre le ridicule de l’entreprise vu
    l’inégalité du rang social et des rapports de force.
    - « En troisième crime, tu me prends pour un imbécile. » Crime de tous les
    patriciens que Mereia va payer…

3. Quoi qu’il réponde, le destin de Mereia n’a d’autre issue que la mort ce que
   signale l’emploi du futur de certitude. : « Cela fait deux crimes, et une alternative
   dont tu ne sortiras pas.»
   
   En l’absence de réaction des autres personnages, Caligula est à la fois le
   plaignant et le procureur : il décide du statut de ce crime imaginaire et adapte sa
   sanction qu’il présente comme une preuve de respect et ironiquement pour
   récompenser sa révolte et en faire un signe de son honneur (perdu) : « De ces trois
   crimes, un seul est honorable ».

4. Le fait qu’il soit ironiquement assimilé à un « révolté », à un « meneur d’hommes »
   rend le personnage encore plus pathétique tant le contraste est grand avec la
   réalité…C’est d’ailleurs ce qui lui est implicitement reproché par Caligula sui a déjà
   gracié Cherea : son manque de courage, d’idéal : « Tu vas mourir virilement pour
   t’être révolté »p 94.
   
   Sans avocat pour plaider sa cause, Mereia est ridiculisé, incapable de se
   défendre, ses réponses hésitantes et tronquées traduisent son trouble. Seul et
   désarmé devant la violence de Caligula, il en est presque à reconnaître sa
   culpabilité : « Oui … Je veux dire … Non »/ « mon asthme… »/  « Caius ! / Elle
   est…rigoureuse, Caius.» La ponctuation traduit la tension émotionnelle qui envahit
   le personnage, presque en état de sidération. Il est incapable de développer une
   argumentation convaincante face à la logique du discours impérial : « Mais elle ne
   s’applique pas au cas ».
   
   Mereia est réduit au silence, on lui fait endosser une personnalité qui n’est pas la
   sienne : il refuse le suicide et s’accroche à la vie « Mereia secoué de sanglots, refuse
   de la tête. » : il a perdu la parole et toute chance de survie.

5. La réponse de Caligula à cette attitude est sans appel, le champ lexical de la
   violence rend la scène particulièrement spectaculaire et d’ailleurs difficile à
   réaliser sur scène par son côté romanesque (le cinéma serait plus approprié) : «
   Mais Caligula, d’un bond sauvage, l’atteint au milieu de la scène, le jette sur un
   siège bas et, après une lutte de quelques instants, lui enfonce la fiole entre les
   dents et la brise à coups de poing. Après quelques soubresauts, le visage plein
   d’eau et de sang, Mereia meurt. Caligula se relève et s’essuie machinalement. »
   p94. Là encore, Camus point e le comportement animal de Caligula vu comme un
   prédateur mais la brutalité de la scène peut également exprimer une véritable rage
   intérieure de ne trouver personne à la mesure de sa démesure : « l’élève Mereia »
   n’a rien compris aux leçons de son maître. La réaction finale de Caligula est
   surprenante, cette rage est en fait une colère froide et ce meurtre est non seulement
   l’affirmation de sa liberté et de son pouvoir sans limite mais semble aussi constituer
   un véritable règlement de compte avec une certaine humanité…
   
   Dans cette scène, l’injustice est institutionnalisée puisqu’il s’agit d’une
   décision impériale. Or on voit bien que la peine de mort, sanction légale, n’est
   autre qu’un assassinat (Camus est contre la peine de mort et l’exprimera dans son
   roman L’Etranger : cf. préface). Elle est la conclusion logique d’une justice dévoyée
   au service de l’individu, au service du bon plaisir du tyran et non au service du bien
   commun.

#### d) Le mépris de la vie humaine.

On voit bien que la vie humaine n’a pas vraiment d’importance pour l’empereur. Le
qualificatif d « ’insecte », appliqué à Mereia, montre le peu de considération de
Caligula pour la vie humaine mais rappelle la puissance de l’empereur et
l’insignifiance des autres citoyens. Rabaissée au rang de l’animalité, sa vie ne
vaut rien.

Ce peu de considération pour la vie humaine, relayé par le peu de réactivité de
Caesonia (« avec calme » p 94, est lisible dans sa répartie fataliste alors que Mereia
est innocenté : « CALIGULA, regardant Mereia, après un silence. Cela ne fait rien.
Cela revient au même. Un peu plus tôt, un peu plus tard … Il sort brusquement,
d’un air affairé, en s’essuyant toujours les mains. » p94 La mort n’est rien : elle n’a
pas plus de sens que la vie. Pour Caligula les actes sont donc vains, rien n’a de
conséquence, car tout homme finit par mourir. Sans remords, sans compassion,
Caligula peut ici apparaître comme un monstre.

Si on a l’impression que l’empereur s’identifie au destin, il suit implacablement
la logique d’une réflexion qui s’inscrit dans le cadre de sa découverte après la
mort de Drusilla : « Les hommes meurent et ils ne sont pas heureux. » (I, 4 p49).

Mais dans le cas de Caligula, cette pensée radicale et désespérée, presque
romantique, ne conduit qu’à la barbarie…Plus rien n’a de valeur, tout est égal.Dans
cette scène, on peut dire que Caligula réinvente sa vie, fait l’expérience pleine et
entière de sa liberté mais le prix à payer est extrêmement lourd et aboutit à la fin de
tout dialogue, à la déshumanisation.

#### Bilan

Cette scène est particulièrement terrifiante car elle souligne la précarité dans
laquelle vivent les citoyens sous le règne de Caligula. Le monstre endormi se réveille
soudain et attaque sans crier gare mais on ne peut prévoir ses caprices pour
anticiper ses coups de griffes. Les victimes se trouvent à la fois démunies et
abandonnée à la violence par la lâcheté de l’entourage, sans doute soulagé
d’échapper encore une fois à l’arbitraire et à la cruauté du pouvoir.

Dans cette scène 10 de l’acte II, on voit que Caligula cherche à aller au bout
de sa folie en montrant qu’il a tous les droits sur ses sujets. Cette scène met
particulièrement en valeur la réaction des différents personnages.

### III - Face à la violence de Caligula: accepter ou mourrir

En effet, cette scène où la violence montre crescendo ne fait que confirmer
davantage l’impuissance des patriciens dont la posture oscille entre flatterie et retrait
silencieux. Personne n’ose affronter le tyran et s’opposer à sa cruauté. Le discours
dominant est d’abord celui de la collaboration.

#### a) Les complices de Caligula, ses porte-parole se calquent d’ailleurs sur le discours et la volonté de Caligula et se font le relai fidèle du tyran même délirant. :

##### 1) Le discours est également péremptoire et assuré : 

« Cherea : je ne
vois pas le rapport/ Caesonia : il y est pourtant » p 90/ « le citoyen qui n’a pas
obtenu de décoration au bout de douze mois est exilé ou exécuté » Le ton
didactique nous montre bien qu’il s’agit d’une loi générale qui s’applique à tous
alors qu’elle récompense absurdement la débauche et oblige à l’excès.
Caesonia ne commente pas cette loi, elle ne fait que l’édicter et abdique ainsi
toute vision critique mais en même temps toute personnalité : elle n’est plus
que l’ombre du tyran.

Une demande de justification de la part d’un patricien obtient une
réponse justificative qui commence bien par « parce que » mais elle rapporte
les propos de Caligula : « Caligula dit que ça n’a aucune importance ».On ne sait
plus qui parle : Caesonia ? Caligula ?

Quant à Hélicon, il fait l’éloge de la loi tout en pointant ironiquement
l’impuissance des patriciens face au caprice du tyran :
Le ton d’Hélicon est certainement ironique quand il répond aux éloges
éhontés des patriciens qui collaborent à la folie de Caligula et approuvent sa
solution de renflouement du Trésor public. Il souligne leur lâcheté par une
antiphrase reposant sur deux hyperboles décalées par rapport à la réalité de la
loi: « Et toujours de façon très morale. Remarquez-le bien. ».

Là encore, la logique semble imparable : « Il vaut mieux, après tout taxer
le vice que rançonner la vertu comme on le fait dans les société républicaine. »
Sauf qu’il oublie de préciser que c’est l’Etat, l’Institution qui pousse au vice…

**Bilan :** Complètement aliénés, jouissant de l’impunité que leur confère leur
proximité avec le tyran, Caesonia et Hélicon ne sont pas à même de freiner la
démesure de Caligula et de s’opposer à sa pulsion destructrice. Au contraire, et
surtout pour Hélicon, rester dans le sillage du tyran, c’est aussi la possibilité
d’exercer le pouvoir.

#### b) Face à la terreur : l’attitude ambiguë des patriciens :

##### 1) Une démonstration de passivité qui semble confirmer la soumission.

Alors que Caligula impose sa volonté, la déstabilisation de la plupart des
personnages se traduit par leur silence ou une expression hésitante, peu
structurée:

Le discours et la posture de Mereia illustre cette caractéristique :

« Alors pourquoi m’as-tu fait rester ? », « Mon asthme », « Oui … Je veux dire …
Non » (répliques encore plus courtes, points de suspension, revirements).

Lorsqu’il apprend sa condamnation à mort, sa position est d’abord passive et
crispée comme le suggère la didascalie p94: « Pendant tout ce discours, Mereia se
rapetisse peu à peu sur son siège. ». S’identifiant à son statut de victime, ses
réactions n’ont rien d’héroïque : « secoué de sanglots, refuse de la tête ». Au lieu de
faire front, il « tente de s’enfuir », attitude gesticulatoire complètement inefficace.

D’autre part, alors qu’il y a d’autres personnages sur scène : Lepidus (dont il a
fait mourir le fils (II, sc.5 p81) et Octavius (sc.9). On les entend peu :
l’intervention du « troisième patricien » (Octavius ? cf distribution des rôles)
reste anecdotique. Lorsque les choses se gâtent avec Mereia, les didascalies
indiquent que Cherea et Caesonia ont « gagné le fond de la scène ». De même,
Lepidus suit le dialogue mais n’intervient pas p 93. Face à la montée de la
violence, la plupart des personnages restent passifs : ils reflètent finalement
l’humanité ordinaire. Leur inertie les rend complices du tyran.

##### 2) La collaboration plus ou moins volontaire des patriciens : jeu de Cherea.

Le jeu de Cherea est plus trouble. En effet, ce dialogue se déroule dans une
atmosphère de terreur et pousse à l’hypocrisie. Cherea cherche à échapper à
la vindicte du tyran et à gagner du temps, notamment pour organiser un coup
d’Etat. Ce qui le conduit à tenir le discours de la collaboration et de la lâcheté.
Quitte à sacrifier Mereia…

Ainsi, sur la question du jour imposée par Caligula, les patriciens ne s’avancent
pas directement:

- Cherea semble le plus défiant et cherche à savoir sa position sur le sujet :
« Qu’est-ce qui ne va pas ? Le personnel est-il mauvais ? ». Il ne critique pas
frontalement le discours de l’empereur et indique simplement : « Je ne vois
pas le rapport. » pour temporiser.
- Mereia qui n’a pas compris le jeu de Caligula est tombé dans le piège en
proposant trop vite une solution trop évidente : « Il faut augmenter les tarifs ».
Il se fait humilier et s’étonne naïvement : « Alors pourquoi m’as-tu fais
rester ?». il s’annonce d’emblée comme la prochaine victime du tyran.

Cherea prend immédiatement le contre-pied de Mereia dont il se désolidarise
pour faire mine d’approuver Caligula : « …je dirais qu’il ne faut pas toucher les
tarifs. ». Mais ne s’avance pas sur la solution laissant le champ libre à la parole
de l’empereur.

Il choisit ensuite la flatterie par un discours exagérément élogieux qui apparaît
peu sincères vu le décalage avec l’iniquité et l’absurdité de la loi proposée, mais
qui reconnaît le génie de Caligula (comme génie du Mal ?) : « C’est lumineux. »/
« Bravo. Le Trésor public est aujourd’hui renfloué. ». Comme dans le discours
de Winston dans l’extrait de 1984, l’absence de points d’exclamation traduit le
manque d’enthousiasme de Cherea !!! Mais il en va de sa survie.

**Bilan :** Dans cette scène 10 de l’acte 2, Camus représente deux visions de
l’homme, toutes deux pessimistes. D’un côté, celle des patriciens qui ont perdu
leur honneur et leur dignité parce qu’ils ont perdu leur liberté. De l’autre, celle de
Caligula, qui montre les dangers de l’hubris (la démesure, en grec, perdre son
humanité en pensant qu’on est un dieu) et qu’il peut façonner le monde à son
image : dans les deux cas, on aboutit à une forme d’anti-humanisme et à la
barbarie.

### Bilan

Cette scène met en relief le retournement de situation qui fait des
patriciens, des marionnettes manipulées par Caligula et ses sbires. On pourrait
rire de ce revirement de fortune mais on rit jaune car la vengeance est terrible
et s’exerce aveuglément, voire injustement. La toute-puissance de Caligula face
à l’impuissance des autres personnages finit ainsi par donner à cette scène une
tonalité particulièrement tragique. On ne voit pas comment mettre fin à cette
folie furieuse. Aucune résistance ne semble possible.
