---
layout: post
title: "«L'Étranger», Albert Camus, 1942"
excerpt: "Le narrateur, Mersault, fait le récit d'une partie de sa vie. Il a grandi en Algérie"
author: fliife
category: francais
tags: fr3
---

**Texte étudié**:

- Incipit du roman <u>L'Étranger</u>.
- Le narrateur, Mersault, fait le récit d'une partie de sa vie. Il a grandi en Algérie, qui occupe
  une place importante dans le récit. Dans l'extrait, Mersault raconte la nouvelle de la mort de sa mère
  et les préparatifs pour la cérémonie et l'enterrement.


> Cet incipit correspond-il à l’horizon d’attente du lecteur/aux attentes du lecteur ?

# Plan proposé

- **I - Forme du récit déroutante**
    + **a) genre ?**
        - Bilan: Mersault n’a donc pas beaucoup de recul par rapport à ce qu’il raconte
    + **b) Écriture qui pose question**
        - Bilan: On ne sait pas quel statut donner à ce récit
- **II - Manque d'informations dans l'incipit**
    + **a) Repérage temporel**
        - Bilan: Mersault ne se projette pas dans l’avenir
    + **b) Repérage spacial**
        - Bilan: Impression: le personnage ne s’intéresse à rien
    + **c) liens du narrateur avec son entourage peu détaillés**
        - Bilan: Le narrateur ne décrit jamais les liens qu’il peut avoir avec les autres personnages
- **III - Un personnage déstabilisant, et peu sympathique**
    + **a) Infifférence du perso face au décès**
        - Note: *Il s’agit pour Mersault de suivre au minimum les codes sociaux*
        - Bilan: Le récit exhibe le côté imposé et dépersonnalisé du rituel social qu’est l’enterrement
    + **b) Interactions avec les autres personnages soulignent sa froideur**
    + **c) Finalement, les seules notations un peu intimes et prosonelles concernent les sensations**
        - Bilan: le personnage est surtout observateur et qu’il vit les événements de l’extérieur, «en étranger»
- **Conclusion**
    + Ouverture: «Malheur à qui veut garder son originalité  !», Rhinocéros, monologue final: la société sanctionnera

## I - Forme du récit déroutante

### a) Quel genre ?

Ouverture sur une phrase choc: *«Aujourd'hui maman est morte»* → Journal intime ou journal de bord car 
mise en relief du temps et passé composé → Style oral et familier → fait penser au discours.

Utilisation d'adverbes de temps reliés au présent du narrateur: *«aujourd'hui», «demain», «après-demain»*, …

> **Mersault n'a donc pas beaucoup de recul par rapport à ce qu'il raconte**

### b) Écriture qui pose question

Narration fragmentée:

- Première partie semble entrer dans la conscience de Mersault qui relate le déroulement de la matinée:
    + *«Je prendrai l'autobus à deux heures et j'arriverai dans l'après-midi»*
    + *«J'ai demandé deux jours de congé à mon patron.»*
- Après une **ellipse narrative**, le personnage raconte son réci au ***passé composé***
    + Analepse qui coupe le début du récit de voyage: *«J'ai pris l'autobus à deux heures»*)
        - **Brouille la cronologie** → Donne un côté **spontané** = entretien de l'illusion qu'il s'agit d'une écriture perso

Il parait étrange que le narrateur se précise à lui-même la localisation de l'Asile: *«Marengo»*. L'ensemble reste ambigu.

> **On ne sait pas quel statut donner à ce récit**

## II - Manque d'informations dans l'incipit

L'ensemble est flou et est souvent cantonné à l'implicite. Le cadre spatio-temporel est flou.

### a) Repérage temporel:

Malgré l'insistance sur le temps (*«Hier» «demain»*, ...), le repérage temporel est limité.

Nous n'avons aucune information passée à cause de l'entrée *in medias res*.

La mort de sa mère est floue (cause ? âge ?). On sait simplement qu'elle est morte en semaine.

L'avenir se limite à un ou deux jours

> **Mersault ne se projette pas dans l'avenir.**

### b) Repérage spatial

Espace peu décrit (restaurant de céleste ?).

Pendant son voyage, le personnage dort → évite la description du paysage.

Le manque d'infos peut s'expliquer par la focalisation interne et par le style du journal de bord:
le scripteur d'un journal ne décrirait pas ce qu'il connait déjà

> **Impression: le personnage ne s'intéresse à rien**

### c) Liens du narrateur avec son entourage peu détaillés

Illusion de réel dans l'écriture causée par l'abscence d'artifices pour faire connaître
son identité (physique, morale). On ne devine que son statut d'employé dans une entreprise.

Le personnage ne parle ni de son père, ni de sa mère dont on ne connait finalement que son décès.
Ses amis sont désignés par leur prénoms mais ne sont pas décrits ni différenciés → profondeur des
liens qui les unissent ?

De même, la relation patron-employé est strictement limitée au travail.

> **Le narrateur ne décrit jamais les liens qu'il peut avoir avec les autres personnages**

### Bilan

L'auteur ne s'appuis sur aucun des procédés du roman réaliste (excepté pour le début *in medias res*).

**Crédibilité due à l'adoption du style du journal de bord** → Engendre la frustration chez le lecteur

La technique du journal de bord suscite aussi des attentes chez le lecteur: on s'attend à ce que le
personnage nous fasse part des ses sentiments. Là encore, l'incipit est déroutant.

## III - Un personnage déstabilisant, et peu sympathique

### a) Infifférence du perso face au décès

Le narrateur ne montre aucun sentiment. Seul signe d'affectivité : *«maman»*, mais il ne semble pas
regretter d'ignorer quand sa mère est morte: ses interventions son sèches, il ne réagit pas à 
l'annonce de la mort mais à l'annonce de la date; il ne faudrait pas qu'il rate l'enterrement mais
cela ne le panique pas → **sang-froid**: la **raison** l'emporte sur **l'émotion** (cf voc de la raison).

Kilométrage Alger-Marengo (l.4), planification du voyage, recherche d'efficacité pour rentrer vite
(*«Ainsi, je pourrai veiller et rentrer demain soir»*) : **pas de lamentation, mais des calculs.**

> **Il s'agit pour Mersault de suivre au minimum les codes sociaux**

Volonté de respecter ces codes:

- Modalisateurs (*«Il a fallu»* l.15)
- Attention au respect des usages (*«La veillée», «la cravatte noire», «le brassard»* l.15).
    + Peut choquer le lecteur car suggère que le narrateur ne tenait que peu à sa mère.

Le récit exhibe le **côté imposé et dépersonnalisé du rituel social qu'est l'enterrement** par le
fait que **les mêmes attributs du deuil vont servir pour rendre hommage à un autre mort** (*«Il a
perdu son oncle»*).

Néanmoins, le personnage **ne fait pas semblant d'être triste**. D'ailleurs, le décès de sa mère
lui semble être une excuse pour un jour de congé suplémentaire (l.6-7, «Excuse»,
l.11 «affaire classée», **euphémisme**).

### b) Interactions avec les autres personnages soulignent sa froideur

- La seule trace de sentiment réside dans son rapport avec son patron: *«Il n'avait pas l'air
  content»*. Il sagit d'une **culpabilité purement professionnelle, qui n'a rien d'affectif**. <br>
  Les relations sont fondées sur le strict respect des codes sociaux: *«C'était plutot à lui
  de me présenter ses condoléances»*.
- Il ne modifie pas ses habitudes, et se rend au café pour déjeuner, «comme d'habitude». Sa
  froideur **contraste avec la réaction des autres personnages** qui expriment des sentiments:
  «ils avaient tous de la peine pour moi». <br> En revanche, le dialogue met en valeur le
  registre didactique de ce type de discours, **cliché** qui suppose l'attachement **obligatoire**
  du fils à sa mère. Le personnage ne répond pas à cette intervention.
- Cependant notre personnage semble aussi avoir du mal à communiquer avec les autres, ce
  qu’induit son comportement avec le militaire : *«J’ai dit « oui » pour ne plus avoir à parler.»*
  (l.20-21)
- Le personnage nous met d’autant plus mal à l’aise que son style d’écriture reste basique et
  repose sur des phrases simples donnant l’impression que tout est sur le même ton et donc que
  tous les événements de la journée sont mis sur le même plan sans que le narrateur s’implique
  personnellement, sans qu’il se sente à un moment donné concerné ou touché.

### c) Finalement, les seules notations un peu intimes et prosonelles concernent les sensations.

«il faisait très chaud » (l.12)/ « j’étais un peu étourdi »(l.14)/ les « cahots »/ « l’odeur
d’essence » (l.18)/ « la réverbération de la route et du ciel »(l.18). Le personnage semble plutôt
centré sur son corps et ses réactions sont physiologiques où interprétées dans ce sens : pris par
la précipitation (« j’ai couru »/ « cette hâte, cette course » (l.17)) le narrateur se laisse envahir
pas ses sensations. La relation de cause à effet (« c’est à cause de tout cela sans doute…que je
me suis assoupi » (l.17-18)) induit tout de même un sentiment de culpabilité et le fait qu’il sente
que sa posture pourrait paraître déplacée et aurait besoin d’être justifiée. Mais ce sentiment de
honte reste implicite et n’est qu’une hypothèse. Il n’est jamais exprimé clairement et on ignore
s’il est d’origine affective ou sociale.

Cette abscence de subjectivité marquée fait que le personnage se contruit surtout par son
comportement et donne l'impression que le personnage est surtout observateur et qu'il vit
les événements de l'extérieur,  «en étranger»: du coup, la notion de pdv est également brouillée

## Conclusion

Comme le personnage ne correspond pas aux attentes culturelles et sociales du lecteur,
ce dernier ne peut pas vraiment rentrer en empathie avec lui. Son comportement est presque celui
d’un marginal. Il apparaît d’emblée comme une sorte de monstre, un « étranger » auquel nous ne
pouvons pas vraiment nous identifier. Pourtant, sa posture inhabituelle nous interpelle sur
l’expression de nos propres sentiments : sommes-nous toujours sincères ? Nos sentiments sont-ils
toujours profonds ? L’expression de ces sentiments est-elle commandée par des attentes sociales et
non par une véritable émotion, un réel besoin de s’épancher ? Le personnage deviendrait alors une
sorte de révélateur effrayant du conformisme et de la pauvreté de nos comportements et de nos
émotions. Nous serions pratiquement tout le temps et en toute circonstance…étrangers …à nous-
mêmes…et « Malheur à celui qui veut garder son originalité ! » (Rhinocéros, monologue final) : la
société le sanctionnera.

Meursault, comme l’affirme Camus, est celui qui refuse de jouer « le jeu » social jusqu’au bout.
