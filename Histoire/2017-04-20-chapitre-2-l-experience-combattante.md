---
layout: post
title: "Chapitre 2: L'expérience combatante dans la première guerre mondiale (1914-1918)"
excerpt: "Le choix de l'arimiste le 11 Novembre 1918 n'est pas un hasard"
author: fliife
category: histoire
---

Le choix de l'armistice (le 11 Novembre 1918) n'est pas un hasard: on commémore la «fin de l'enfer», et non pas la fin de la guerre. (28 Juin 1919 = Traité de Versailles).
Le XXème siècle est celui de l'industralisation et de l'apparition de la guerre totale.

*Problématique: **Quelles sont les nouvelles formes de combat qui apparaissent durant la Première guerre mondiale ?***

## L1: La première guerre mondiale annonce la guerre moderne

### I - Des alliances au déclenchement de la guerre

En Europe, il existe deux alliances militaires préventives:

- La **Triple Entente:**  France, Royaume-Uni, Empire Russe
- La **Triple Alliance:** Empire Allemand, Empire Austro-Hongrois, Italie

Le 28 Juin 1914 et l'attentat de Sarajevo (Bosnie) déclenche l'engrenage des alliances: en Septembre 1914, l'Europe est en guerre.

### II - Guerre de mouvement puis guerre de position

La guerre de mouvement de dure que quelques mois (l'armée Allemande passe par la Belgique puis la France jusqu'à la Marne). En Décembre 1914, le Front occidental se stabilise des Vosges jusqu'à la frontière Suisse (environ 800km).

Cependant, après trois années de guerre de position (carnage humain, qui tue des milliers de soldats en quelques heures pour avancer de quelques dizaines de mètres, comme, par exemple, Verdun, 1916, où sont morts plus de 300 000 soldats et 400 000 furent blessés). En 1917, l'absurdité de ces combats provoquent des grèves et des mutineries durement réprimées (éxecutions pour l'exemple).

### III - La victoire des alliés

En 1917, tout change:

- Le 2 avril 1917, les USA rentrent en guerre
- En Décembre 1917, La Russie se retire de la guerre (traité de Brest-Litovsk) à cause de la révolution Bolchevique (Octobre 1917 par Lénine)

Au printemps 1918, la guerre de mouvement reprend avec l'arrivée de l'armée américaine et d'une nouvelle arme de Renault (les chars d'assaut).

En Août 1918, la Triple Entente perce le front. À Berlin, l'empereur Guillaume II abdique le 3 Novembre, puis le 11 Novembre le nouveau gouvernement Allemand signe l'armistice.

## L2: Les violences durant la première guerre mondiale

*Problématique: **Quelles violences ont marqué les esprits ?***

### I - L'expérience combattante

Ce qui a le plus traumatisé les soldats:

- La vie dans les tranchées est très difficile (boue, froit, rats, poux, puanteur, aucune hygiène) *(= Les poilus)*
- Dans les combats: 
    + La guerre d'usure (tactique qui consiste à épuiser l'ennemi par tous les moyens)
    + Les bombardements qui rendent fous
    + Le pilonnage (déluge de fer et de feu)
    + La sortie des tranchées face aux nouvelles armes (mitrailleuses, mortiers, lance-flamme, gaz, le corps à corps, les cadavres partout)
    + La brutalisation (Soldats deviennent des machines à tuer au contact de la guerre, processus de déshumanisqtion)
    + Gueules cassées

Cependant, des mutineries ont eu lieu en 1917.

### II - Les civils

Selon leur position géographique, les civils ont plus ou moins souffert de la guerre:

- À proximité du Front: Les bombardements détruisent aussi villes et villages (ex: Champagne/npdc)
- Travail forcé
- Déportations
- Pénuries
- Violences:
    + Génocide (= Extermination préméditée et organisée d'un peuple pour des raisons religieuse ou idéologiques) Arménien: l'Empire Ottoman qui participe à la guerre aux côtés de la Triple Alliance subit de nombreuses défaites. Ce sont leur généraux nationalistes qui reprochent aux arméniens d'être respondables. Les Arméniens sont donc déportés vers le Sud (actuelle frontière Syrienne). La déportation se transforme en extermination systématique. Au total, il y a entre 1,2 et 1,5 millions de victimes. En 1973, l'ONU reconnait le génocide.

## L3: La guerre totale

> **Guerre totale:** Guerre dans laquelle l'État mobilise tous les moyens (économiques, humains, culturels, religieux, politiques, militaires).

*Problématique: **Comment s'effectue la mobilisation de tous les moyens ?***

### I - Les moyens humains

Pendant la première guerre mondiale, tous les peuples sont concernés, plus ou moins directement: les hommes deviennent soldats au combat, d'abord les Européens, mais aussi les colonies (par exemple, pour la France, l'Indochine, le Maghreb). Il y a environt 600 000 soldats qui viennent des colonies Françaises.
Les femmes remplacent les hommes partis au combat dans les usines — les «*munitionnettes*» — mais aussi dans les campagnes. La France utilise aussi la main d'oeuvre coloniale

### II - Les moyens financiers

La guerre est longue, coûteuse en matériel de guerre, en achat de matières premières, ce qui oblige les États à emprunter régulièrement de l'argent auprès des civils. L'éconimie de guerre coûte cher et force les États à emprunter à l'exterieur (ex: banques américaines). Ces emprunts vont se répercuter en Europe après la guerre: augmentation des impôts, forte inflation monétaire (baisse de la valeur de l'argent, augmentation des prix des produits)

### III - Les esprits

Dans tous les étaits belligérants (qui se battent), l'Union Sacrée est acceptée (le temps de la guerre, on enterre tous conflits politiques et religieux)

L'État met en place une propagande publique qui exalte le patriotisme (récupérer l'Alsace-Moselle, la censure dans la presse pour ne pas démoraliser les civils, et le bourrage de crân: en réaction, «Le Canard Enchaîné» est créé en 1915.

Une culture de guerre se met en place: dans les esprits, la guerre produit de nouvelles représentations (le héro = le poilu, le monstre = le soldat allemand)
