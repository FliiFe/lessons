---
layout: post
title: "Chapitre 3: Les regimes totalitaires de l'entre-deux guerres"
excerpt: "Quelle est la genèse des régimes totalitaires ? Quels sont leur points communs ? "
author: fliife
category: histoire
---

*Problematique: **Genèse ? Points communs ? Différences entre les trois ?***

<table><thead><tr>
<th>Extrême gauche</th>
<th>Extrême droite</th>
</tr></thead><tbody><tr>
<td>- Communisme sovietique</td>
<td>- Fascisme</td>
</tr><tr>
<td></td>
<td>- Nazisme</td>
</tr></tbody></table>

Pour les trois régimes politiques, les extrêmes se fondent sur les inégalités sociales, le chomâge, la crise économique, et la frustration sociale.

> **Régime**: forme ou organisation politique

> **Régime totalitaire**: régime dans lequel un homme ou un parti contrôle la population de la naissance à la mort, 24h/24

> **Dictature**: régime politique d'un homme ou d'un parti qui supprime systematiquement les opposants

> **Genèse**: origine

## L1: La genèse des régimes totalitaires

Point commun: la PGM déclenche les trois processus

### I - Le rôle de la PGM

**En russie:** la PGM est durement vecue par la population russe (défaites sur le front Ouest, rationnement et pénuries).
La population commence à protester, surtout Petrograde (capitale imperiale après Moscou), les manifestations se radicalisent: le Tsar doit abdiquer en mars 1917 et laisse place a un gouvernement provisoire et a la création des soviets (assemblées du peuple)

**En Italie:** Les troubles d'apres-guerre: frustration, humiliation → "blousés", "paix mutililée"

Les alliés n'ont pas tenu leur promesse de rendre toutes les terres ex-autrichiennes a l'Italie. La Dalmatie n'est pas recup. De plus, l'agitation sociale parcours l'Italie, surtout dans les villes : cela provoque grèves, chomage et violences. La bourgeoisie italienne a peur de la montée du communisme.

**En Allemagne**: La nouvelle république de Weimar est accusée de trahison car elle a accepté toutes les conditions des Alliés: une partie des allemands considère que c'est une humiliation (*Diktat*, traite de versaille). Dans les grandes villes, les violences sont organisées (spartakistes, 1919). Extrême droite: Tentative de putsch (coup d'état) d'Hitler en 1923

### II - les accessions au pouvoir

**En Russie:** 2eme révolution (celle d'octobre 17), organisée par lenine  (bolchevique, communiste). Il chasse du pouvoir le gouvernement provisoire et installe le communisme immédiatement: il propose de confisquer et de partager les terres, ce qui provoque la guerre civile entre les russes et les bourgeois (?)

**En Italie:** Acces par la légalite: Mussolini (fasciste) rassure les bourgeois avec des escadrons paramilitaires appelés *faisceaux de combat* ("chemises noires") qui agissent en brisant les grèves et en exercant des violences dans la rue (contre les ouvriers et les syndicalistes). 1921: creation du PNF

**En Allemagne**: Par la légalite: apres l'échec de Munich en 1923, les escadrons paramilitaires d'Hitler — les *sections d'assaut* — appliquent la violence de rue contre les communistes, les ouvriers, ce qui rassure aussi la bourgeoisie allemande. 

La crise des années 30 favorise le parti nazi dans les élection (NSDAP = Parti National Socialiste des Travailleurs Allemands). Le parti nazi devient le premier parti en 1932. le 30/01/33, Hitler est choisi comme nouveau chancelier par le président Hindenburg.

### III - L'instauration des dictatures

**En Russie:** Lénine applique immédiatement le programme communiste, c'est-à-dire la «dictature du proletariat» (violences, execution des opposants). (**Proletaire**: ouvriers, paysans, pauvres du peuple). Cela mène a une guerre civile entre les russes Blancs (partisans du Tsar) et les russes Rouges (soviets). Victoire de Lénine en 1921. Un an après, création de l'Union des Republiques Socialistes Sovietiques (URSS). Pendant quelques annees, il adopte une pause economique, c'est-à-dire, un retour partiel au capitalisme = «NEP», la Nouvelle Politique Economique. Apres la mort de Lénine en 1924, le communisme va se durcir avec l'arrivée de Staline au pouvoir.

**En Italie:** Mussolini donne le change pendant deux ans en créant un gouvernement de coalition (gauche + centre + droite). Mais les
élections de 1924 sont truquées → victoire du parti fasciste. Assasinat du principal opposant, le socialite Matteoti en 1924, revendiqué par Mussolini. La dictature commence en 1925-1926 avec le vote des lois fascissime (parti unique, libertes interdites). Mussolini a tous les pouvoirs.

**En Allemagne**: Hitler se débarrasse immediatement des communistes par l'incendie du Reichstag (33). Le Parti Communiste est accusé et est interdit. Les élections sont supprimées, ainsi que les libertés. Parti unique: NSDAP. en 1934, Hitler prend les pleins-pouvoirs.

## L2: Les idéologies fascistes, nazis et communistes

*Problématique: **Quels sont les 3 discours et leur problématique ?***

> **Idéologie:** C'est un discours politique, religieux, moral, économique, ... tenu par une personne ou un parti politique, dont le but est d'influencer (de convaincre) la société

### I - En commun : le rejet de la démocratie

Le peuple de doit plus être une force politique mais doit se fondre dans l'État nazi ou fasciste ou dans le parti communiste. En conséquence, la démocratie, le parlementarisme sont nuisibles à l'État ou à l'utilité Nationale. Le droit individuel est donc supprimé, les lois aussi, sauf celles qui émanent de la volonté du chef ou du parti

### II - En commun: la rupture avec le passé et l'avènement de l'Homme nouveau

Pour le communisme, l'Homme Nouveau, est le travailleur (l'ouvrier — marteau — et le paysan — fauscille —) libéré du capitalisme, c'est à dire de l'exploitation des bourgeois. L'État communiste doit supprimer le capitalisme, socialiser les moyens de production (première étape: contrôle ouvrier dans les usines, contrôle paysan sur les terres). Cette révolution terminée, l'État disparaitra pour laisser place à une société communiste (sans classe sociale) où le bonheur règnera.

Pour le nazisme, l'Homme nouveau est l'aryen (prétendue race germanique supérieure) qui doit combattre pour protéger sa pauvreté contre les Juifs, puis les étrangers en général. Pour sa supériorité, il doit asservir les races inférieures (les slaves), étendre son espace vital, réunir en un seul pays tous les peuples d'origine germanique en un grand Reich. Le principal ennemi politique qu'il faut détruire est le communisme. Ici, la vision nazi de l'Homme nouveau est raciste.

Pour le fascisme, l'Homme nouveau est l'Italien qui a retrouvé sa force, son héritage romain, sa capacité à se battre et à se sacrifier pour son chef. Mais dans les années 1930, il devient aussi antisémite avec le rapprochement Mussolini-Hitler.

### III - Des objectif différents

L'idéologie communiste se distingue par son universalité, c'est le bonheur de l'Homme qui est avancé et pas celui d'une nation ou d'un peuple.

À l'inverse, le nazisme et le fascisme rejettent toute l'idée d'égalité, exaltent la puissance des forts sur les faibles (c'est la «loi de la nature»). Il n'y a pas de projet de conquête pour le communisme contrairement à Mussolini qui veut reconstituer l'Empire Romain, tandis qu'Hitler veut conquérir l'Europe de l'Est puis la suprématie mondiale qui passe par la destruction du peuple Juif.

## L3: Les totalitarismes au quotidien

*Problématique: **Qui contrôle la société ? Comment ?***

### I - Un chef, un parti, un État

**En Allemagne:** «Ein Volk, ein Reich, ein Führer» (Un peuple, un empire, un guide). C'est l'expression même du culte de la personalité que met en place Hitler (la propagande basée sur des cérémonies, des défilés, la radio, le cinéma, les discours enflamés). le symbole de la soumission au chef est le «*salut Hitlérien*»

**En Italie:** «Le chef a toujours raison». Même culte du chef, avec les mêmes moyens de propagande.

**En URSS:** Le culte du chef s'organise sur une autre image plus sympathique («*Le Petit Père des Peuples*» = surnom de Staline, qui a continué la révolution. Même propagande.

Dans les 3 cas, c'est le parti qui organise la propagande (NSDAP, PNF, PCUS)

### II - Le contrôle des économies

En Allemagne et en Italie, les économies restent privées mais contrôlées pour être au service de l'État. Ce sont des économies de guerre (les grands travaux, industries d'armement) qui préparent la guerre et qui doit garantir l'autarcie (autonomie). Ex: en Italie, les «batailles» des années 1930 (bataille du blé, bataille des naissances). En Allemagne, le plan quadriennal en 1936.

Seule l'économie soviétique ne prépare pas la guerre mais est totalement contrôlée par Staline. En 1929, il impose le «grand tournant», c'est l'imposition par la force de l'économie communiste: disparition de la propriété privée (dékoulakisation des campagnes) (Koulak = paysan riche). La dékoulakisation est transformée par la collectivisation (prise en charge des campagnes par tous les paysans organisées en fermes d'État). L'État communisme organise des plans quinquennaux.

### III - Les moyens pour contrôler la population

**La propagande:** Elle est omniprésente et utilisa tous les moyens techniques modernes (radio, cinéma, mises en scène des discours qui enthousiasme les foules)

**Le corporatisme:** C'est la mise en groupe des populations selon leur travail, leur age dans le but de casser les syndicats (Kolkhose: exploitation agricole collective, Sovkhose: ferme d'État, Stackhanovisme: la travailleur zélé qui se surpasse pour la révolution). Le but est de faire disparaître les différences sociales, leurs rivalités, et favoriser le sentiment d'appartenance à une seule nation qui s'identifie au chef et à l'État. Ex: Allemagne avec les «Jeunesses Hitlériennes»

**La terreur:** Dans les trois États totalitaires, une police politique est créée dans le but de pourchasser, arrêter, supprimer et déporter les opposants (Allemagne = Gestapo, URSS = KGB, Italie = OVRA). L'internement des opposants se fait dans des camps de concentration (ex: Dachau, Goulag en Sibérie). Cas particulier nazi: la législation raciste à partir des lois de Nuremberg (1935) → Hitler peut appliquer son projet antisémite en excluant les juifs allemands de la société
