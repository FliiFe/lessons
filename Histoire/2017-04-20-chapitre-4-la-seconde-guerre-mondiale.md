---
layout: post
title: "Chapitre 4: La seconde guerre mondiale"
excerpt: "Quels sont les différents aspects de la guerre d'anéantissement ? "
author: fliife
category: histoire
---

***Quels sont les différents aspects de la guerre d'anéantissement ?***

## L1: Les étapes de la seconde guerre mondiale

***Quelles sont les grandes phases de la seconde guerre mondiale ?***

### I - Les victoires de l'Axe (<u>Axe</u> = Allemagne + Italie + Japon), 1939-1942

> **Rappel**: Le pacifisme (sentiment dominant en Europe dans les années 1920-1930) est un échec (SDN: Incapable de freiner Hitler).

-   <u>1er septembre 1939</u>: Invasion surprise de la Pologne par Hitler à l'Ouest et l'URSS à l'Est (pacte germano-soviétique de 1939)
-   <u>3 septembre 1939</u>: France + Royaume-uni déclarent la guerre à Hitler.

Victoire d'Hitler car il adopte la stratégie de la **Blitzkrieg**: combinaison des armes modernes. D'abord des avions qui bombardent, puis l'arivée des divisions blindées et enfin arrivée de l'infanterie pour achever la destruction.

En deux ans, les armées allemandes conquièrent l'Europe du Nord puis du Sud (Armistice en France: <u>22 juin 1940</u>). Pour le Royaume-uni c'est la bataille d'angleterre qui commence en <u>septembre 1940</u> et qui se termine en <u>mai 1941</u> (15000 morts).

### II - La guerre devient mondiale

Le front s'étend aussi à la méditéranée, particulièrement au Nord de l'Afrique.

Le grand changement provient de l'entrée en guerre de l'URSS (avec l'opération Barbarosa) du <u>22 juin 1941</u>

À la suite de l'attaque surprise du japon sur la base navale de Pearl Harbor le <u>7 septembre 1941</u>, les USA entrent en guerre.

La guerre devient mondiale avec d'un côté les USA + l'URSS + le Royaume-uni, et de l'autre l'Allemagne + l'Italie + le Japon.

### III - Du tournant de la guerre à la défaite de l'Axe

En 1942, la situation bascule en faveur des alliés:

-   Dans le pacifique, victoire de Midway (USA vs Japon) le <u>5 et 6 juin 1942</u>
-   En Égypte, victoire des alliés en <u>novembre 1942</u> (El-Alamein). En même temps, les Alliés débarquent en Afrique du Nord.
-   À l'Est, début de la bataille de Stalingrad, en <u>septembre 1942</u> puis victoire soviétique en <u>février 1943</u>.

L'assaut final est organisé en 1944: c'est l'opération Overlord, un débarquement anglo-américain le <u>6 juin 1944</u> sur les plages de Normandie.

Le 3ème Reich est pris en étau. Paris est libéré le <u>25 août 1944</u> puis la jonction se fait en Allemagne le <u>26 avril 1945</u>.

Berlin est prise le <u>30 avril 1945</u>, et capitule sans conditions le <u>8 mai 1945</u>

Le japon refuse la défaite et préfe se battre jusqu'à la mort (→ Kamikazes). Les USA décident d'utiliser la bombe atomique (président = TRUMAN) le <u>6 et 9 août 1945</u>, respectivement à Hiroshima et Nagasaki.

## L2: Le génocide juif et l'extermination des Tziganes

### I - La vision raciste du monde par rapport aux juifs

Le nazisme a tranformé la religion juive en une race qui porte tous les défauts
humains (fourberie, avarice, complots) et qui représente un danger pour les
autres races. Le livre *Mein Kampf* contient déjà l'idée de génocide.

### II - De l'exclusion à l'enfermement

Ce sont les deux premières étapes de la Shoah (= génocide juif). En Allemagne, Hitler au pouvoir commence par exclure les juifs de la sociéte (<u>1935</u> = lois de Nuremberg = série d'interdictions pour les exclure et les humilier, interdiction de pratiquer certains metiers, de se marier, ...) → De nombreux boycotts contre les commerces juifs.

L'enfermement commence en 1933 par les camps de concentration, mais c'est surtout avec la guerre que l'enfermement de masse s'étend à toute l'Europe, dans les grandes villes européennes occupées. Des grandes rafles (arrestations de masse) sont organisées.

La première repression de masse a lieu en 1938 dans toute l'Allemagne: la *Nuit de Crystal*. C'est un Pogrom (massacres de juif depuis le moyen-âge).

### III - La guerre permet l'extermination de masse

La pologne connait la première Shoah par balle. Des sections de SS spécialisées se chargent de ces massacres (les *Einsatzgrupen*). Environ 1 000 000 de juifs d'Europe sont massacrés de cette manière.

L'extermination est d'abord expérimentale (handicapés, ...)

Tout s'accellère en 1942 lors de la conférence de Wansee, où est évoquée la *Solution finale*, qui organise l'extermination des juifs a grande échelle. Elle prévoit:

-   La construction de camps de concentration (camps dans lesquels les déportés meurent par le travail forcé et les mauvais traitements) et de camps d'extermination (camps équipés de chambres à gaz et de fours crématoires) souvent cachés du public.
-   Organisation de ghettos dans les grandes villes d'Europe centrale pour y parquer les juifs (forte mortalité à cause des maladies, de la famine).
-   Organisation de convois de trains pour amener les juifs des ghettos vers les camps d'extermination (conditions épouventables, wagons à bestiaux).

Dans les camps, l'extermination est permanente: les nazis doivent aller vite.

#### Étude du camps d'Auschwitz, *usine de la mort*

Auschwitz était le camps le plus efficace. Étapes:

-   Arrivée du convoi (souvent la nuit)
-   Accueil brutal, triage des vivants et des cadavres
-   Sélection: Des colonnes sont formées: une pour les travailleurs et les autres qui vont directement à l'extermination.
-   Gazage: les juifs sont déshabillés, rasés. On leur indique les salles de douches. Les chambres à gaz utilisent le zyklon B. Après le gazage, les cadavres sont délestés de leurs dents en or ou en argent et sont brûlés dans des fours crématoires. Les cendres sont utilisées comme engrai sur les champs voisins ou répandus dans le camps comme anti-dérapant

## L3 - La seconde guerre mondiale, une guerre d'anéantissement.

***Quelles nouvautés la seconde guerre mondiale présente-t-elle ?***

### I - Le rôle de l'idéologie

Deux idéologies s'opposent:

-   Le nazisme et le fascisme, qui ont pour but d'établir un *«Ordre nouveau»* imposé par la violence. De plus, le nazisme ajoute une vision raciale de la guerre avec la lutte à mort contre le «complot judéo-bolchevique»
-   L'idéologie des Alliés basée sur la defense des droits de l'Homme et la défense de la démocratie. Pour la défendre, les Alliés sont prêts à tout pour détruire l'ennemi.

### II - Une guerre totale

Les états utilisent tous les moyens (économiques, scientifiques, techniques et
humains). Sur le plan géographique, la quasi totalité des états est impliquée.

Exemples:

-   Invention du moteur à réaction, du radar, d'armes sophistiquées, arme nucléaire.
-   Implication des civils:
    +   Du côté Allié: tout le monde travaille pour la guerre.
    +   Du côté de l'Axe: Le travail est forcé (STO = **S**ervice de **T**ravail **O**bligatoire) en 1943.
-   *«Victory Program»* de Roosevelt en 1942: planifie l'économie Américaine, pour construire l'armée de la victoire.

### III - Anéantir les civils

Cette guerre touche surtout les civils sous plusieurs formes:

-   Les bombardements sur l'Allemagne (certaines villes sont rasées), au Japon avec la bombe A (Hiroshima et Nagasaki) = désastre pour l'humanité mais justifié par les Américains.
-   Les massacres organisés (en France: Oradour sur Glane le <u>10 juin 1944</u>)
-   Les viols collectifs (ex: l'armée Japonaise enlève entre 20 000 et 200 000 jeunes filles: *«Femmes de réconfort»*)
-   Les massacres de grande ampleur liés au génocide juif et à l'extermination des Tziganes.
