---
layout: post
title: "Chapitre 1: Croissance et Mondialisation depuis 1850"
excerpt: "Sur quoi repose l'économie mondiale ? Pourquoi y a-t-il des crises économiques ? "
author: fliife
categories: histoire
---

> <u>1850</u>: L'occident s'industrialise puis part coloniser l'Afrique et l'Asie

> **Croissance:** Forte augmentation des activités économiques (secondaires et tertiaires)

> **Mondialisation:** C'est le siècle (XIX) durant lequel l'Asie et l'Afrique entrent dans le système commercial mondial. De plus, la domination économique mondiale va passer de l'Europe aux États-Unis (1918).

*Problématique:*

- ***Sur quoi repose l'économie mondiale ?***
- ***Pourquoi y a-t-il des crises économiques ?***
- ***Quel est le lien entre l'industrialisation et la colonisation ?***

## Introduction

Ce sont 150 ans de croissance économique qui ont profondément transformé les sociétés en créant des économies-monde et en ayant une influence mondiale (ex: culture = anglais utilisé partout, la culture américaine, ..)
Conséquences: 3 étapes de la mondialisation:

- XIX, le Royaume Uni domine l'économie mondiale
- XX, après 1918, les USA dominent l'économie mondiale
- vers 1970, La Triade domine l'économie mondiale. (Triade = Europe du NO, Amérique du Nord, Chine-Japon = états émergents).

## L1: Croissance et crises: les phases économiques de 1850 à nos jours.

*Problématique: **Quelles sont les causes de la croissance économique ?***

### I - Les cycles successifs

Même si la croissance économique est forte entre 1850 et aujourd'hui, elle est constituée d'une succession de périodes de croissance et de dépression. Ce sont des cycles successifs.

<table>
<tr>
<td>1848 — 1873</td>
<td> Première révolution Industrielle (ou industrialisation) <br>→ Charbon, machine à vapeur (textile, métalurgie).</td>
</tr>
<tr>
<td>1896 — 1929</td>
<td> Deuxième révolution Industrielle<br>→ Pétrole, moteur à explosion (automoblie, aviation).</td>
</tr>
<tr>
<td>1945 — 1973</td>
<td> <i>«Les Trente Glorieuses», de Jean Fourastié</i><br>→ Forte croissance des pays occidentaux</td>
</tr>
</table>

Depuis 1973, il y a une croissance dépressive, c'est-à-dire une croissance générale, mais dans le détail — surtout en Asie — alors que la crise continue en occident.

La dernière période de croissance est 1990-2008, grâce à la généralisation d'Internet et de la net-economy. La crise de 2008 est provoquée par la spéculation financière: crise des *Subprimes*. Cette crise est mondialisée à cause d'Internet.

La première crise boursière à New York, le «crash boursier de *Wall Street*» (NSYE, New York Stock Exchange), 1929

### II - L'effet des crises

Chaque crise peut être économique ou politique:

- Crises politiques: Les deux guerres mondiales, qui entrainent des dépressions
- Crises économiques: 
    + Crash boursier de 1929 suivi d'une depression puis de la seconde guerre mondiale.
    + Les deux chocs pétroliers: 
        * 1973: Les pays arabes exportateurs de pétrole décident de fermer le robinet de pétrole, ce qui provoque une explosion des prix pétroliers
        * 1979: La révolution Iranienne provoque l'explosion des prix du pétrole

Conséquences sociales de ces crises: Faillite des entreprises, puis la généralisation du chômage. Cela entraine un apauvrissement, ce qui favorise la montée des extrêmes politiques.

- 2008: Crise *Subprimes*: ce sont des prêts bancaires accordés d'abord aux USA, à des clients non-solvables sur des achats de maisons pour relancer le BTP. D'autres banques européennes le fonc. Les banques s'endettent, mais transforment discrètement leur dettes en actions «pourries» qu'elles revendent sur les marchés boursiers. La bulle financière éclate pendant l'été 2008, entrainant la faillite de plusieurs banques puis d'entreprises puis d'États: la Grèce, l'Espagne, l'Irlande. En 2009, la crise devient mondiale, provoquant aussi des émeutes de la faim au Maroc et au Mexique.

#### Étude de cas: la crise de 1929

C'est la première crise mondiale qui paralyse, durant des années, la planète. Elle a été provoquée par la spéculation boursière sans aucun contrôle des banquiers. La crise éclate le jeudi 24 octobre 1929 (le «*Black Thursday*») = crash boursier de la bourse de *Wall Street*.

Immédiatement, c'est la ruine des actionnaires puis les faillites bancaires (à cause du retrait brutal de l'argent des clients), puis les faillites des entreprises industrielles et agricoles. Conséquences sociales: chômage et misère. La crise devient mondiale car les États-Unis retirent leurs capitaux du reste du monde. (cas particulier: France et Japon qui n'empreintaient que peu aux États Unis). L'effondrement est causé par la baisse des ventes et de l'exportation. La relance économique commence en 1932. Le chomage atteint son apogée en 1932, sauf pour la France qui est le pays industriel le moins touché par la crise. Les populations sont profondément touchées (chomage, misère et soupe populaire). On voit aussi des manifestations appelées «les marches de la faim».

## L2: Comment s'organisent les économies-monde successives ?

### I - Le Royaume Uni, première économie monde de 1850 à 1914

Les facteurs de la réussite économique:

- *Facteur technique:* Le berceau de la première industrialisation, le pionnier industriel
- *Facteur géopolitique:* Le plus grand empire colonial du monde («*L'empire sur lequel le soleil ne se couche jamais*»), ⅕ des territoires émergés, ¼ de l'humanité
- Le Commonwealth = association des anciennes colonies de peuplement (Australie, Canada, Nouvelle Zélande)
- *Facteur maritime:* Marine marchande britannique assure 60% du trafic mondial
- *Facteur financier:* Là ou se trouve la capitale financière mondiale, *Londres* (représente plus de 50% des capitaux investis dans le monde). La seule monnaie mondiale indexée sur l'or est la livre Sterling

Le New Deal du président Roosevelt est un programme novateur axé sur la relance économique par des inverstissements de l'État: financement de grands travaux: barrages, autoroutes, ports.

Keneysianisme (de l'économiste anglais Keyne): Idée que l'État doit intervenir dans l'économie pour la relancer par des investissements (ex: grands travaux)

Autres courants de pensée liés à la croissance économique:

- Libéralisme: Se base sur l'économie de marché (offre et demande). Limitation du rôle de l'état et liberté d'entreprendre.
- Capitalisme: Repose sur la propriété privée des moyens de production (terres, ...) et d'échanges. But = profit.
- Marxisme: Marx, économiste Allemand et théoricien de la lutte des classes et du socialisme. Prolétaires vs Possédants.

### II - Les caractéristiques de l'économie-monde des USA

Même si la puissance étasunienne s'affirme après 1918, c'est surtout après 1945 qu'ils deviennent une hyperpuissance (= un État qui possède tous les aspects de la puissance). En 1945, le grand vainqueur de la Seconde Guerre Mondiale se double d'un grand vainqueur politique et financier: les USA ont financé la seconde guerre mondiale et la victoire, et finance la reconstruction de l'Europe (plan Marshall, 1947). Ils créént les grandes institutions économiques mondiales:

- FMI: Fond Monétaire International (argent prêté aux États qui entrent dans le libre-échange) — 1944
- GATT (O.M.C à partir de 1995): Organisation Mondiale du Commerce, créée en 1947, ces institutions permettent de réorganiser les échanges mondiaux.

Puissances des USA:

- *Puissance technologique:* travail à la chaîne (Fordisme, Tailorisation = invente l'organisation scientifique du travail)
- *Puissance financière:* bourses NYSE et NASDAQ (haute technologie)
- *Puissance culturelle:* le mode de vie américain s'exporte partout, c'est ce qu'on appelle le soft-power. Pour les États-Unis, la culture est une marchandise.
- *Puissance militaire:* Hard-power (Première guerre mondiale, Guerre froide, années 1990-2008 -> Gendarmes du monde)

Cette puissance repose essentiellement sur le rôle des FMN et FTN (Firmes Multi/Trans Nationales). Ex: *Coca Cola, McDonald's*

### III - Mondialisation multipolaire

***Quels sont les caractéristiques de l'économie-monde multipolaire ?***

Trois pôles dominent le monde (La Triade):

- Amérique du Nord
- Europe de l'Ouest
- Asie du Sud-Est

Ce sont des espaces d'inégale puissance. Cependant, depuis une vingtaine d'années, d'autres États ont connu une rapide croissance, surtout en Asie (les pays émergents: PE -> BRICS). Le Brésil est un grand exportateur de produits agricoles (la «*ferme du monde*»), l'Inde exporte des services informatiques (le «*bureau du monde*»), la Chine exporte des produits manufacturés (l'«*usine du monde*») et l'Afrique du Sud exporte des minerais et de la haute technologie.

Les NPIA (Nouveaux Pays Industrialisés d'Asie) depuis les années 1980: Taïwan, Hong-Kong, Singapour et la Corée du Sud

Les bébé-tigres sont les pays nouveaux pays ateliers asiatiques: Vietnam, Thaïlande.

Au total, l'économie-monde multipolaire continue à pernettre à certains pays d'entrer dans la mondialisation. Ce phénomène a commencé avec la révolution des transports: apparition des conteneurs, des bateaux géants, des télécommunications et du système de production «just-in-time» (c'est-à-dire fabriquer sans faire de stocks, en respectant les delais à l'echelle planétaire)

Cette troisième étape a mis en place une spécialisation par état pour répondre aux décideurs de la Triade (c'est la DIT: Division Internationale du Travail)
